package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String again = "";   //Play again? (y or n)
        long seed = System.nanoTime();
        Random r = new Random(seed);
        int pile1, pile2, pile3;
        boolean end;
        String nameSelves;  //Do players want to name themselves (y or n)
        String player1, player2;  //player names ("Player 1," Player 2" by default)
        String pile;  //A, B, or C
        int numRemove;
        Scanner s = new Scanner(System.in);
        System.out.println("Let's play Nims!");
        System.out.println("Would you like to name yourselves? Enter y or n.");
        nameSelves = s.next();
        //Make sure user entered y or n
        while (!nameSelves.equalsIgnoreCase("y") && !nameSelves.equalsIgnoreCase("n")) {
            System.out.println("Invalid input. Please enter y or n.");
            nameSelves = s.next();
        }
        //Players name selves
        if (nameSelves.equalsIgnoreCase("y")) {
            System.out.println("Enter the name for player 1:");
            player1 = s.next();
            System.out.println("Enter the name for player 2:");
            player2 = s.next();
        }
        //Default player names
        else {
            player1 = "Player 1";
            player2 = "Player 2";
        }


        do {
            end = false;  //Is game over?

            //Each pile has between 1 & 15, inclusive
            pile1 = r.nextInt(14) + 1;  //pile A
            pile2 = r.nextInt(14) + 1;  //pile B
            pile3 = r.nextInt(14) + 1;  //pile C

            do {
                //player1's turn
                    System.out.println("\n" + player1 + "'s turn!");
                    //print piles
                    System.out.print("A: ");
                    for (int i = 0; i < pile1; i++) {
                        System.out.print("* ");
                    }
                    System.out.print("(" + pile1 + ")");
                    System.out.print("\n");
                    System.out.print("B: ");
                    for (int i = 0; i < pile2; i++) {
                        System.out.print("* ");
                    }
                    System.out.print("(" + pile2 + ")");
                    System.out.print("\n");
                    System.out.print("C: ");
                    for (int i = 0; i < pile3; i++) {
                        System.out.print("* ");
                    }
                    System.out.print("(" + pile3 + ")");
                    System.out.print("\n");

                    //only one pile is not empty
                    if (pile1 != 0 && pile2 == 0 && pile3 == 0) {
                        System.out.println("Piles B and C are empty. You'll have to remove from pile A.");
                        pile = "A";
                    } else if (pile2 != 0 && pile1 == 0 && pile3 == 0) {
                        System.out.println("Piles A and C are empty. You'll have to remove from pile B.");
                        pile = "B";
                    } else if (pile3 != 0 && pile1 == 0 && pile2 == 0) {
                        System.out.println("Piles A and B are empty. You'll have to remove from pile C.");
                        pile = "C";
                    }

                    //at least 2 piles have items left
                    //get pile name & validate A, B, or C
                    else {
                        System.out.println("Which pile would you like to remove from?");
                        pile = s.next();
                    }
                    while (!pile.equalsIgnoreCase("A") && !pile.equalsIgnoreCase("B") && !pile.equalsIgnoreCase("C")) {
                        System.out.println("Invalid input. Enter A, B, or C.");
                        pile = s.next();
                    }
                    //if A is the only empty pile
                    if (pile.equalsIgnoreCase("A") && pile1 == 0 && pile2 != 0 && pile3 != 0) {
                        System.out.println("Pile A is empty. Please choose pile B or C.");
                        pile = s.next();
                        while (!pile.equalsIgnoreCase("B") && !pile.equalsIgnoreCase("C")) {
                            System.out.println("Invalid input. Enter B or C.");
                            pile = s.next();
                        }
                    }
                    //if B is the only empty pile
                    else if (pile.equalsIgnoreCase("B") && pile2 == 0 && pile1 != 0 && pile3 != 0) {
                        System.out.println("Pile B is empty. Please choose pile A or C.");
                        pile = s.next();
                        while (!pile.equalsIgnoreCase("A") && !pile.equalsIgnoreCase("C")) {
                            System.out.println("Invalid input. Enter A or C.");
                            pile = s.next();
                        }
                    }
                    //if C is the only empty pile
                    else if (pile.equalsIgnoreCase("C") && pile3 == 0 && pile1 != 0 && pile2 != 0) {
                        System.out.println("Pile C is empty. Please choose pile A or B.");
                        pile = s.next();
                        while (!pile.equalsIgnoreCase("A") && !pile.equalsIgnoreCase("B")) {
                            System.out.println("Invalid input. Enter A or B.");
                            pile = s.next();
                        }
                    }

                    //get # to remove & validate  0 < numRemove < # in pile
                    System.out.println("How many would you like to remove?");
                    numRemove = s.nextInt();
                    switch (pile) {
                        case "A":
                        case "a": {
                            while (numRemove < 1 || numRemove > pile1) {
                                System.out.println("Invalid input. Enter an integer from 1 to " + pile1 + ".");
                                numRemove = s.nextInt();
                            }
                            pile1 -= numRemove;
                        }
                        break;
                        case "B":
                        case "b": {
                            while (numRemove < 1 || numRemove > pile2) {
                                System.out.println("Invalid input. Enter an integer from 1 to " + pile2);
                                numRemove = s.nextInt();
                            }
                            pile2 -= numRemove;
                        }
                        break;
                        case "C":
                        case "c": {
                            while (numRemove < 1 || numRemove > pile3) {
                                System.out.println("Invalid input. Enter an integer from 1 to " + pile3);
                                numRemove = s.nextInt();
                            }
                            pile3 -= numRemove;
                        }
                    }

                    //game over if player1 removes last item
                    if (pile1 == 0 && pile2 == 0 && pile3 == 0) {
                        //Play again? Validate y or n
                        end = true;
                        System.out.println("\nGame over! " + player2 + " wins!\n\nWould you like to play again? Please enter y or n.");
                        again = s.next();
                        while (!again.equalsIgnoreCase("y") && !again.equalsIgnoreCase("n")) {
                            System.out.println("Invalid input. Enter y or n.");
                            again = s.next();
                        }
                }

                //player2's turn (only if player1 didn't remove last item)
                if (!end) {
                    System.out.println("\n" + player2 + "'s turn!");

                    //print piles
                    System.out.print("A: ");
                    for (int i = 0; i < pile1; i++) {
                        System.out.print("* ");
                    }
                    System.out.print("(" + pile1 + ")");
                    System.out.print("\n");
                    System.out.print("B: ");
                    for (int i = 0; i < pile2; i++) {
                        System.out.print("* ");
                    }
                    System.out.print("(" + pile2 + ")");
                    System.out.print("\n");
                    System.out.print("C: ");
                    for (int i = 0; i < pile3; i++) {
                        System.out.print("* ");
                    }
                    System.out.print("(" + pile3 + ")");
                    System.out.print("\n");

                    //only one pile is not empty
                    if(pile1 != 0 && pile2 == 0 && pile3 == 0){
                        System.out.println("Piles B and C are empty. You'll have to remove from pile A.");
                        pile = "A";
                    }
                    else if(pile2 != 0 && pile1 == 0 && pile3 == 0){
                        System.out.println("Piles A and C are empty. You'll have to remove from pile B.");
                        pile = "B";
                    }
                    else if(pile3 != 0 && pile1 == 0 && pile2 == 0){
                        System.out.println("Piles A and B are empty. You'll have to remove from pile C.");
                        pile = "C";
                    }
                    else {
                        System.out.println("Which pile would you like to remove from?");
                        pile = s.next();
                    }
                    while (!pile.equalsIgnoreCase("A") && !pile.equalsIgnoreCase("B") && !pile.equalsIgnoreCase("C")) {
                        System.out.println("Invalid input. Enter A, B, or C.");
                        pile = s.next();
                    }
                    //if A is the only empty pile
                    if(pile.equalsIgnoreCase("A") && pile1 == 0 && pile2 != 0 && pile3 != 0){
                        System.out.println("Pile A is empty. Please choose pile B or C.");
                        pile = s.next();
                        while (!pile.equalsIgnoreCase("B") && !pile.equalsIgnoreCase("C")) {
                            System.out.println("Invalid input. Enter B or C.");
                            pile = s.next();
                        }
                    }
                    //if B is the only empty pile
                    if(pile.equalsIgnoreCase("B") && pile2 == 0 && pile1 != 0 && pile3 != 0){
                        System.out.println("Pile B is empty. Please choose pile A or C.");
                        pile = s.next();
                        while (!pile.equalsIgnoreCase("A") && !pile.equalsIgnoreCase("C")) {
                            System.out.println("Invalid input. Enter A or C.");
                            pile = s.next();
                        }
                    }
                    //if C is the only empty pile
                    if(pile.equalsIgnoreCase("C") && pile3 == 0 && pile1 != 0 && pile2 != 0){
                        System.out.println("Pile C is empty. Please choose pile A or B.");
                        pile = s.next();
                        while (!pile.equalsIgnoreCase("A") && !pile.equalsIgnoreCase("B")) {
                            System.out.println("Invalid input. Enter A or B.");
                            pile = s.next();
                        }
                    }
                    System.out.println("How many would you like to remove?");
                    numRemove = s.nextInt();
                    switch (pile) {
                        case "A":
                        case "a": {
                            while (numRemove < 1 || numRemove > pile1) {
                                System.out.println("Invalid input. Enter an integer from 1 to " + pile1 + ".");
                                numRemove = s.nextInt();
                            }
                            pile1 -= numRemove;
                        }
                        break;
                        case "B":
                        case "b": {
                            while (numRemove < 1 || numRemove > pile2) {
                                System.out.println("Invalid input. Enter an integer from 1 to " + pile2 + ".");
                                numRemove = s.nextInt();
                            }
                            pile2 -= numRemove;
                        }
                        break;
                        case "C":
                        case "c": {
                            while (numRemove < 1 || numRemove > pile3) {
                                System.out.println("Invalid input. Enter an integer from 1 to " + pile3 + ".");
                                numRemove = s.nextInt();
                            }
                            pile3 -= numRemove;
                        }
                    }
                    ;
                    if (pile1 == 0 && pile2 == 0 && pile3 == 0) {
                        end = true;
                        System.out.println("\nGame over! " + player1 + " wins!\n\nWould you like to play again? Please enter y or n.");
                        again = s.next();
                        while (!again.equalsIgnoreCase("y") && !again.equalsIgnoreCase("n")) {
                            System.out.println("Invalid input. Enter y or n.");
                            again = s.next();
                        }
                    }
                }
            }while (!end) ;

        } while (again.equalsIgnoreCase("y"));
    }
}
