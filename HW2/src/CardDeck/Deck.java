package CardDeck;
import java.util.*;
import CardDeck.Card;

/**
 * Created by kwhite on 7/12/2017.
 */
public class Deck {

    LinkedList<Card> ll;

    public Deck() {
        this.ll = new LinkedList<Card>();
        for (Card.Suit s : Card.Suit.values()) {
            for (Card.Face f : Card.Face.values()) {
                Card card = new Card(s, f);
                this.ll.add(card);
            }
        }
    }

    public void shuffle(int numTimes) {
        for (int i = 0; i < numTimes; i++) {
            long seed = System.nanoTime();
            Random random = new Random(seed);
            Collections.shuffle(this.ll, random);
        }
    }

    public Card drawCards() {
        if(this.ll.size() == 0) {
            throw new Error("The deck is empty!\n");
        }
        Card card = this.ll.remove(0);
        return card;
    }

    public void returnCards(LinkedList<Card> LL) {
        for (int i = LL.size()-1; i >= 0; i--) {
            this.ll.addFirst(LL.get(i));
        }
    }

    public String toString() {
        System.out.printf(this.ll.size() + " cards currently in the Deck: ");
        String cards = "";
        for (int i = 0; i < this.ll.size(); i++) {
            cards = cards + this.ll.get(i).toString() + " ";
        }
        return cards;
    }
}