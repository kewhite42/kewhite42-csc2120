//I'm assuming that if player & dealer stand on consecutive turns, whoever has the higher card value wins.

package com.company;
import CardPlayer.*;
import CardDeck.Card;
import CardDeck.Deck;
//import java.util.Scanner;
import util.Keyboard;


public class Main {

    public static void main(String[] args) {
        boolean again = true;
        Keyboard kb = Keyboard.getKeyboard();
        CardGame cardGame;

        //Which card game to play? Validate input.
        String input = kb.readString("Would you like to play blackjack(b) or go fish(f)? Please enter b or f.\n").toLowerCase();
        while(!input.equals("b") && !input.equals("f")){
            input = kb.readString("Invalid input. Enter b for blackjack or f for go fish.\n").toLowerCase();
        }

        if(input.equals("b")) {
            System.out.println("Let's play some blackjack!");
            String name = kb.readString("Would you like to name yourself? Please enter y or n.\n").toLowerCase();
            while (!name.equals("y") && !name.equals("n")) {
                name = kb.readString("Invalid input. Enter y or n.\n").toLowerCase();
            }

            boolean end = false;
            while (again) {
                double money = kb.readDouble("How much money are you playing with?\n" +
                        "Don't be reckless. Enter a dollar amount less than $1.75 x 10^308.\nAmount: $");
                while (money < 0) {
                    money = kb.readDouble("Invalid input. Enter a nonnegative number.");
                }
                double bet = kb.readDouble("How much money do you bet? Again, less than $1.75 x 10^308.\nBet: $");
                while(bet < 0)
                {
                    bet = kb.readDouble("That's a negative number.\n" +
                            "Unless you want to lose money when you win and earn it when you lose, enter a nonnegative number.\nBetting amount: $");
                }
                while(bet > money){
                    bet = kb.readDouble("Curious, seeing how you only have $" + money + ".\n" +
                            "Your bet must be at most the amount of money you're playing with.\nBetting amount: $ ");
                }
                if (name.equals("y")) {
                    String playerName = kb.readString("Enter your name.\n");
                    cardGame = new Blackjack(playerName, money, bet);
                } else
                    cardGame = new Blackjack(money, bet);

                while (!end) {
                    end = cardGame.takeTurns();
                }
                again = cardGame.playAgain();
            }
        }

        else{
                System.out.println("Let's play some go fish!");
                int numPlayers = kb.readInt("How many players are there? Enter a number from 2 to 6.\nNumber of players: ");
                while (numPlayers < 2 || numPlayers > 6) {
                    numPlayers = kb.readInt("Invalid input. Enter an integer between 2 and 6, inclusive.\nNumber of players: ");
                }

                String name = kb.readString("Would you like to name yourselves? Please enter y or n.\n").toLowerCase();
                while (!name.equals("y") && !name.equals("n")){
                    name = kb.readString("Invalid input. Enter y or n.\n").toLowerCase();
                }

                if (name.equals("y"))
                    cardGame = new GoFish(numPlayers, true);
                else
                    cardGame = new GoFish(numPlayers, false);

                boolean end = false;
                while(again) {
                    while (!end) {
                        end = cardGame.takeTurns();
                    }
                    cardGame.winner();
                    again = cardGame.playAgain();
                }
            }


    }
}
