package CardPlayer;

import util.Keyboard;
import CardDeck.Card;
import CardDeck.Deck;

import static CardPlayer.BlackjackPlayer.State.BUST;
import static CardPlayer.BlackjackPlayer.State.CAN_HIT;
import static CardPlayer.BlackjackPlayer.State.STAND;

/**
 * Created by kwhite on 7/19/2017.
 */
public class Blackjack extends CardGame {
    BlackjackPlayer player = new BlackjackPlayer();
    Dealer dealer = new Dealer();
    double bet;

    public Blackjack(double money, double b){
        player.name = "Player";
        player.money = money;
        bet = b;
    }

    public Blackjack(String playerName, double money, double b){
        player.name = playerName;
        player.money = money;
        bet = b;
    }

    //player & dealer take turn in one call
    public boolean takeTurns(){
        deal();
        Keyboard kb = Keyboard.getKeyboard();
        while (player.getState() != BUST && dealer.getState() != BUST) {
        System.out.println(player.name + " has: " + player.getHandValue() + " with " + player.getHand());
            String input = kb.readString("Would you like to hit(h) or stand(s)? Please enter h or s.\n").toLowerCase();
            while(!input.equals("h") && !input.equals("s")){
                input = kb.readString("Invalid input. Enter h or s.").toLowerCase();
            }

            if (input.equals("h")) {
                Card c = deck.drawCards();
                player.hit(c);
                if (player.getState() == BUST) {
                    System.out.println("Player busted!\nDealer wins!");
                    System.out.println("Player has: " + player.getHandValue() + " with " + player.getHand());
                    System.out.println("Dealer has: " + dealer.getHandValue() + " with " + dealer.getHand());
                    player.money -= bet;
                    return true;
                }
            }
            else {
                player.stand();
                System.out.println("Player stands!");
                if(dealer.getState() == STAND){
                    if(player.getHandValue() > dealer.getHandValue()){
                        System.out.println("Player wins!");
                        System.out.println("Player has: " + player.getHandValue() + " with " + player.getHand());
                        System.out.println("Dealer has: " + dealer.getHandValue() + " with " + dealer.getHand());
                        player.money += bet;
                        return true;
                    }
                    else{
                        System.out.println("Dealer wins!");
                        System.out.println("Player has: " + player.getHandValue() + " with " + player.getHand());
                        System.out.println("Dealer has: " + dealer.getHandValue() + " with " + dealer.getHand());
                        player.money -= bet;
                        return true;
                    }
                }
                while (dealer.getState() == CAN_HIT) {
                    Card c = deck.drawCards();
                    dealer.hit(c);
                    System.out.println("Dealer hits.");
                    if(dealer.getState() == STAND) {
                        System.out.println("Dealer stands.");
                        if(player.getState() == STAND){
                            if(dealer.getHandValue() >= player.getHandValue()){
                                System.out.println("Dealer wins!");
                                System.out.println("Player has: " + player.getHandValue() + " with " + player.getHand());
                                System.out.println("Dealer has: " + dealer.getHandValue() + " with " + dealer.getHand());
                                player.money -= bet;
                                return true;
                            }
                            else{
                                System.out.println("Player wins!");
                                System.out.println("Player has: " + player.getHandValue() + " with " + player.getHand());
                                System.out.println("Dealer has: " + dealer.getHandValue() + " with " + dealer.getHand());
                                player.money += bet;
                                return true;
                            }
                        }
                    }
                    else if (dealer.getState() == BUST) {
                        System.out.println("Dealer busts!\nPlayer wins!");
                        System.out.println("Player has: " + player.getHandValue() + " with " + player.getHand());
                        System.out.println("Dealer has: " + dealer.getHandValue() + " with " + dealer.getHand());
                        player.money += bet;
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void deal(){
        deck.shuffle(1);
        Card playerCard = deck.drawCards();
        player.addToHand(playerCard);
        Card dealerCard = deck.drawCards();
        dealer.addToHand(dealerCard);
        playerCard = deck.drawCards();
        player.addToHand(playerCard);
        dealerCard = deck.drawCards();
        dealer.hit(dealerCard);
    }

    @Override
    public boolean playAgain(){
        Keyboard kb = Keyboard.getKeyboard();
        if(player.money == 0)
            return false;
        else {
            String again = kb.readString("Would you like to play again? Please enter y or n.\n").toLowerCase();
            while (!again.equals("y") && !again.equals("n")) {
                again = kb.readString("Invalid input. Enter y or n.\n").toLowerCase();
            }
            if(again.equals("y"))
                return true;
            else
                return false;
        }
    }

    public void winner(){

    }
}
