package CardPlayer;

import CardDeck.Card;

/**
 * Created by kwhite on 7/22/2017.
 */
public class GoFishPlayer extends Player {
    protected int numBooks;

    protected int numAces, numTwos, numThrees, numFours, numFives, numSixes,
            numSevens, numEights, numNines, numTens, numJacks, numQueens, numKings;

    public GoFishPlayer(){
        super();
        numAces = 0;
        numTwos = 0;
        numThrees = 0;
        numFours = 0;
        numFives = 0;
        numSixes = 0;
        numSevens = 0;
        numEights = 0;
        numNines = 0;
        numTens = 0;
        numJacks = 0;
        numQueens = 0;
        numKings = 0;
        for(Card c : hand.getCards()){
            String card = c.toString().substring(0,1);
            switch(card){
                case "A": numAces++;
                break;
                case "2": numTwos++;
                break;
                case "3": numThrees++;
                break;
                case "4": numFours++;
                break;
                case "5": numFives++;
                break;
                case "6": numSixes++;
                    break;
                case "7": numSevens++;
                    break;
                case "8": numEights++;
                    break;
                case "9": numNines++;
                    break;
                case "10": numTens++;
                    break;
                case "J": numJacks++;
                    break;
                case "Q": numQueens++;
                    break;
                case "K": numKings++;
            }
        }
    }
}
