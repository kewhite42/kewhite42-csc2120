package CardPlayer;

/**
 * Created by kwhite on 7/22/2017.
 */
import CardDeck.Card;

import java.util.LinkedList;

import static CardPlayer.BlackjackPlayer.State.BUST;
import static CardPlayer.BlackjackPlayer.State.CAN_HIT;
import static CardPlayer.BlackjackPlayer.State.STAND;

public class BlackjackPlayer extends Player {
    public enum State {STAND, CAN_HIT, BUST};
    protected State state;
    protected double money;

    public BlackjackPlayer(){
        this.money = 0;
        this.state = CAN_HIT;
    }

    public LinkedList<Card> newHand(Card a, Card b) {
        LinkedList<Card> ll = this.hand.getCards();
        Hand newHand = new Hand();
        newHand.addCard(a);
        newHand.addCard(b);
        this.hand = newHand;
        return ll;
    }

    public void hit(Card c) {
        if(this.state == CAN_HIT)
            hand.addCard(c);
        if(getHandValue() > 21)
            this.state = BUST;
    }

    public void stand() {
        if(this.state != BUST)
            this.state = STAND;
    }

    public State getState() {
        return this.state;
    }
}
