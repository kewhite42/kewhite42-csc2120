package CardPlayer;

import CardDeck.Card;

import java.util.Collections;
import java.util.LinkedList;

import util.Keyboard;

/**
 * Created by kwhite on 7/19/2017.
 */
public class GoFish extends CardGame {
    protected int numPlayers;
    protected LinkedList<GoFishPlayer> players;

    public GoFish(int numPlayers, boolean nameSelves){
        players = new LinkedList<GoFishPlayer>(Collections.nCopies(numPlayers, null));
        Keyboard kb = Keyboard.getKeyboard();
        this.numPlayers = numPlayers;
        for(int i = 0; i < numPlayers; i++)
            players.set(i, new GoFishPlayer());

        if(nameSelves){
            for(int i = 0; i < numPlayers; i++){
                players.get(i).name = kb.readString("Player " + (i+1) + ", enter your name: ");
            }
        }
        else{
            for(int i = 0; i < numPlayers; i++){
                players.get(i).name = "Player " + (i+1);
            }
        }
    }

    //all players take turn in one call
    public boolean takeTurns() {
        boolean end = false;
        deal();
        Keyboard kb = Keyboard.getKeyboard();
        while (!end) {
            for (GoFishPlayer p : players) {
                System.out.println("\n" + p.name + "'s turn!");
                if (p.handSize() > 0)
                    System.out.println(p.hand.toString());

                //get face of card player wants
                String card = kb.readString("What card do you want?\nCard: ").toUpperCase();
                boolean inHand = false;
                for (int i = 0; i < p.handSize(); i++) {
                    Card c = p.hand.getCards().get(i);
                    if ((c.toString().substring(0, 1).equals(card)) || (c.toString().substring(0, 2).equals("10") && card.equals("10")))
                        inHand = true;
                }
                if (!inHand)
                    card = kb.readString("Invalid input. You must ask for a card that's in your hand.");
                while (!inHand) {
                    for (int i = 0; i < p.handSize(); i++) {
                        Card c = p.hand.getCards().get(i);
                        if ((c.toString().substring(0, 1).equals(card)) || (c.toString().substring(0, 2).equals("10") && card.equals("10")))
                            inHand = true;
                    }
                    if (!inHand)
                        card = kb.readString("Invalid input. You must ask for a card that's in your hand.");
                }
                /*while (!card.equals("A") && !card.equals("2") && !card.equals("3") && !card.equals("4") && !card.equals("5")
                        && !card.equals("6") && !card.equals("7") && !card.equals("8") && !card.equals("9") && !card.equals("10")
                        && !card.equals("J") && !card.equals("Q") && !card.equals("K")) {
                    card = kb.readString("Invalid input. Choose A, 2, 3, 4, 5, 6, 7, 8, 9, 10, J, Q, or K.\nCard: ");
                }*/

                    //get player the player wants to ask for the card
                    int person;
                    if (numPlayers == 2) {
                        if (p.name.equals(players.get(0).name))
                            person = 1;
                        else
                            person = 0;
                    }
                    else {
                        int j = 1;
                        //print menu of players that still have cards
                        for (int i = 0; i < numPlayers; i++) {
                            if (players.get(i).getHandValue() != 0 && !p.name.equals(players.get(i).name)) {
                                System.out.println(j + ". " + players.get(i).name);
                                j++;
                            }
                        }
                        person = kb.readInt("Which player would you like to ask? Enter a number from 1 to " +
                                (j - 1) + ".\nPlayer: ");
                        while (person < 1 || person > (j - 1)) {
                            if (person < 1 || person > (j - 1)) {
                                System.out.println("Invalid input. Enter ");
                                for (int i = 0; i < (j - 2); i++)
                                    System.out.print(i + ", ");
                                System.out.print(", or " + (j - 1) + ".");
                                person = kb.readInt("Player: ");
                            }
                        }
                    }

                    for (int i = 0; i < numPlayers; i++) {
                        if (i == person) {
                            //check person's hand
                            //if found card, remove from person & add to player taking turn
                            boolean found = false;
                            GoFishPlayer play = players.get(i);
                            for (int k = 0; k < play.handSize(); k++) {
                                if (!found) {
                                    Card currCard = play.hand.getCards().get(k);
                                    if ((currCard.toString().substring(0, 1).equals(card)) ||
                                            (card.equals("10") && currCard.toString().substring(0, 2).equals("10"))) {
                                        found = true;
                                        //play.removeFromHand(currCard);
                                        //p.addToHand(currCard);
                                        switch (card) {
                                            case "A": {
                                                play.numAces--;
                                                p.numAces++;
                                            }
                                            break;
                                            case "2": {
                                                play.numTwos--;
                                                p.numTwos++;
                                            }
                                            break;
                                            case "3": {
                                                play.numThrees--;
                                                p.numThrees++;
                                            }
                                            break;
                                            case "4": {
                                                play.numFours--;
                                                p.numFours++;
                                            }
                                            break;
                                            case "5": {
                                                play.numFives--;
                                                p.numFives++;
                                            }
                                            break;
                                            case "6": {
                                                play.numSixes--;
                                                p.numSixes++;
                                            }
                                            break;
                                            case "7": {
                                                play.numSevens--;
                                                p.numSevens++;
                                            }
                                            break;
                                            case "8": {
                                                play.numEights--;
                                                p.numEights++;
                                            }
                                            break;
                                            case "9": {
                                                play.numNines--;
                                                p.numNines++;
                                            }
                                            break;
                                            case "10": {
                                                play.numTens--;
                                                p.numTens++;
                                            }
                                            break;
                                            case "J": {
                                                play.numJacks--;
                                                p.numJacks++;
                                            }
                                            break;
                                            case "Q": {
                                                play.numQueens--;
                                                p.numQueens++;
                                            }
                                            break;
                                            case "K": {
                                                play.numKings--;
                                                p.numKings++;
                                            }
                                        }
                                        play.removeFromHand(currCard);
                                        p.addToHand(currCard);
                                    }
                                }
                            }
                            if(found){
                                System.out.println("Yes! You got a " + card + ".");
                                System.out.println(p.hand.toString());
                            }
                            else {
                                System.out.println(play.name + " doesn't have a " + card + ". Go fish!");
                                Card drawnCard = deck.drawCards();
                                p.addToHand(drawnCard);
                                System.out.println(p.hand.toString());
                            }
                            if (p.numAces == 4) {
                                p.numBooks++;
                                System.out.println("You have a book of aces! Let's set it aside.");
                                for (int l = 0; l < p.handSize(); l++) {
                                    Card bookCard = p.hand.getCards().get(l);
                                    if (bookCard.toString().substring(0, 1).equals("A"))
                                        p.removeFromHand(bookCard);
                                }
                                System.out.println(p.hand.toString());
                            }
                            if (p.numTwos == 4) {
                                p.numBooks++;
                                System.out.println("You have a book of twos! Let's set it aside.");
                                for (int l = 0; l < p.handSize(); l++) {
                                    Card bookCard = p.hand.getCards().get(l);
                                    if (bookCard.toString().substring(0, 1).equals("2"))
                                        p.removeFromHand(bookCard);
                                }
                                System.out.println(p.hand.toString());
                            }
                            if (p.numThrees == 4) {
                                p.numBooks++;
                                System.out.println("You have a book of threes! Let's set it aside.");
                                for (int l = 0; l < p.handSize(); l++) {
                                    Card bookCard = p.hand.getCards().get(l);
                                    if (bookCard.toString().substring(0, 1).equals("3"))
                                        p.removeFromHand(bookCard);
                                }
                                System.out.println(p.hand.toString());
                            }
                            if (p.numFours == 4) {
                                p.numBooks++;
                                System.out.println("You have a book of fours! Let's set it aside.");
                                for (int l = 0; l < p.handSize(); l++) {
                                    Card bookCard = p.hand.getCards().get(l);
                                    if (bookCard.toString().substring(0, 1).equals("4"))
                                        p.removeFromHand(bookCard);
                                }
                                System.out.println(p.hand.toString());
                            }
                            if (p.numFives == 4) {
                                p.numBooks++;
                                System.out.println("You have a book of fives! Let's set it aside.");
                                for (int l = 0; l < p.handSize(); l++) {
                                    Card bookCard = p.hand.getCards().get(l);
                                    if (bookCard.toString().substring(0, 1).equals("5"))
                                        p.removeFromHand(bookCard);
                                }
                                System.out.println(p.hand.toString());
                            }
                            if (p.numSixes == 4) {
                                p.numBooks++;
                                System.out.println("You have a book of sixes! Let's set it aside.");
                                for (int l = 0; l < p.handSize(); l++) {
                                    Card bookCard = p.hand.getCards().get(l);
                                    if (bookCard.toString().substring(0, 1).equals("6"))
                                        p.removeFromHand(bookCard);
                                }
                                System.out.println(p.hand.toString());
                            }
                            if (p.numSevens == 4) {
                                p.numBooks++;
                                System.out.println("You have a book of sevens! Let's set it aside.");
                                for (int l = 0; l < p.handSize(); l++) {
                                    Card bookCard = p.hand.getCards().get(l);
                                    if (bookCard.toString().substring(0, 1).equals("7"))
                                        p.removeFromHand(bookCard);
                                }
                                System.out.println(p.hand.toString());
                            }
                            if (p.numEights == 4) {
                                p.numBooks++;
                                System.out.println("You have a book of eights! Let's set it aside.");
                                for (int l = 0; l < p.handSize(); l++) {
                                    Card bookCard = p.hand.getCards().get(l);
                                    if (bookCard.toString().substring(0, 1).equals("8"))
                                        p.removeFromHand(bookCard);
                                }
                                System.out.println(p.hand.toString());
                            }
                            if (p.numNines == 4) {
                                p.numBooks++;
                                System.out.println("You have a book of nines! Let's set it aside.");
                                for (int l = 0; l < p.handSize(); l++) {
                                    Card bookCard = p.hand.getCards().get(l);
                                    if (bookCard.toString().substring(0, 1).equals("9"))
                                        p.removeFromHand(bookCard);
                                }
                                System.out.println(p.hand.toString());
                            }
                            if (p.numTens == 4) {
                                p.numBooks++;
                                System.out.println("You have a book of tens! Let's set it aside.");
                                for (int l = 0; l < p.handSize(); l++) {
                                    Card bookCard = p.hand.getCards().get(l);
                                    if (bookCard.toString().substring(0, 1).equals("1"))
                                        p.removeFromHand(bookCard);
                                }
                                System.out.println(p.hand.toString());
                            }
                            if (p.numJacks == 4) {
                                p.numBooks++;
                                System.out.println("You have a book of jacks! Let's set it aside.");
                                for (int l = 0; l < p.handSize(); l++) {
                                    Card bookCard = p.hand.getCards().get(l);
                                    if (bookCard.toString().substring(0, 1).equals("J"))
                                        p.removeFromHand(bookCard);
                                }
                                System.out.println(p.hand.toString());
                            }
                            if (p.numQueens == 4) {
                                p.numBooks++;
                                System.out.println("You have a book of queens! Let's set it aside.");
                                for (int l = 0; l < p.handSize(); l++) {
                                    Card bookCard = p.hand.getCards().get(l);
                                    if (bookCard.toString().substring(0, 1).equals("Q"))
                                        p.removeFromHand(bookCard);
                                }
                                System.out.println(p.hand.toString());
                            }
                            if (p.numKings == 4) {
                                p.numBooks++;
                                System.out.println("You have a book of kings! Let's set it aside.");
                                for (int l = 0; l < p.handSize(); l++) {
                                    Card bookCard = p.hand.getCards().get(l);
                                    if (bookCard.toString().substring(0, 1).equals("K"))
                                        p.removeFromHand(bookCard);
                                }
                                System.out.println(p.hand.toString());
                            }
                        }
                    }
                }
            }
            for (GoFishPlayer p : players) {
                if (p.handSize() > 0)
                    end = true;
            }
        return true;
        }

    public void deal(){
        deck.shuffle(1);
        for(int i = 0; i < numPlayers; i++){
            for(int j = 0; j < 5; j++){
                Card c = deck.drawCards();
                players.get(i).addToHand(c);
            }
        }
    }

    public void winner(){
        GoFishPlayer winner = players.get(0);
        for(GoFishPlayer p : players){
            if(p.numBooks > winner.numBooks)
                winner = p;
        }
        System.out.println(winner.name + " wins with " + winner.numBooks + " books!");
    }
}
