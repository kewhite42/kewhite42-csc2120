package CardPlayer;

import CardDeck.Deck;
import util.Keyboard;

/**
 * Created by kwhite on 7/19/2017.
 */
public abstract class CardGame {
    Deck deck = new Deck();
    public abstract boolean takeTurns();  //all players take turn in one call
    public abstract void deal();

    public boolean playAgain(){
        Keyboard kb = Keyboard.getKeyboard();
        String again = kb.readString("Would you like to play again? Please enter y or n.").toLowerCase();
        while(!again.equals("y") && !again.equals("y")){
            again = kb.readString("Invalid input. Enter y or n.").toLowerCase();
        }
        if(again.equals("y"))
            return true;
        else
            return false;
    }

    public abstract void winner();
}
