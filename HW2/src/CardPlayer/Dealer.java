package CardPlayer;

import CardDeck.Card;
import java.util.LinkedList;

/**
 * Created by kwhite on 7/14/2017.
 */
public class Dealer extends BlackjackPlayer {
    public LinkedList<Card> newHand(Card a, Card b) {
        LinkedList<Card> cards = super.newHand(a, b);
        if(getHandValue() < 17)
            this.state = State.CAN_HIT;
        else if(getHandValue() == 17)
            this.state = State.STAND;
        return cards;
    }

    public void hit(Card c) {
        super.hit(c);
        if(getHandValue() >= 17 && getHandValue() <= 21)
            this.state = State.STAND;
        else if(getHandValue() < 17)
            this.state = State.CAN_HIT;
    }
}
