package CardDeck;

/**
 * Created by kwhite on 7/12/2017.
 */
public class Card {
    public enum Suit {C, D, H, S}
    public enum Face {A, TWO, THREE, FOUR, FIVE, SIX,
        SEVEN, EIGHT, NINE, TEN, J, Q, K}

    private Suit suit;
    private Face face;

    public Card(Suit suit, Face face) {
        this.suit = suit;
        this.face = face;
    }

    public Suit getSuit() {
        return this.suit;
    }

    public Face getFace() {
        return this.face;
    }

    public String toString() {
        Card.Face face = this.face;
        Card.Suit suit = this.suit;
        String cardFace;
        String cardSuit;
        if (face == Face.A)
            cardFace = "A";
        else if (face == Face.TWO)
            cardFace = "2";
        else if (face == Face.THREE)
            cardFace = "3";
        else if (face == Face.FOUR)
            cardFace = "4";
        else if (face == Face.FIVE)
            cardFace = "5";
        else if (face == Face.SIX)
            cardFace = "6";
        else if (face == Face.SEVEN)
            cardFace = "7";
        else if (face == Face.EIGHT)
            cardFace = "8";
        else if (face == Face.NINE)
            cardFace = "9";
        else if (face == Face.TEN)
            cardFace = "10";
        else if (face == Face.J)
            cardFace = "J";
        else if (face == Face.Q)
            cardFace = "Q";
        else
            cardFace = "K";

        if (suit == Card.Suit.C)
            cardSuit = "C";
        else if (suit == Card.Suit.D)
            cardSuit = "D";
        else if (suit == Card.Suit.H)
            cardSuit = "H";
        else
            cardSuit = "S";

        return cardFace + cardSuit;
    }
}
