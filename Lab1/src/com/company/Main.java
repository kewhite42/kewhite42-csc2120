package com.company;
import CardDeck.Card;
import CardDeck.Deck;
import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {

        Deck deck = new Deck();
        System.out.printf(deck.toString());
        System.out.printf("\n");
        for (int i = 0; i < 80; i++) {
            System.out.printf("-");
        }
        System.out.printf("\n");

        //draw 5 cards
        LinkedList<Card> LL = new LinkedList<Card>();
        for (int i = 0; i < 5; i++) {
            Card card = deck.drawCards();
            LL.addLast(card);
        }

        System.out.printf("Drawn Cards: ");
        for (int i = 0; i < 5; i++) {
            System.out.printf(LL.get(i).toString() + " ");
        }
        System.out.printf("\n");

        System.out.printf(deck.toString());

        System.out.printf("\n");
        for (int i = 0; i < 80; i++) {
            System.out.printf("-");
        }
        System.out.printf("\n");

        //return cards to deck
        deck.returnCards(LL);

        System.out.printf(deck.toString());
        System.out.printf("\n");
        for (int i = 0; i < 80; i++) {
            System.out.printf("-");
        }
        System.out.printf("\n");

        deck.shuffle(5);

        System.out.printf(deck.toString());
        System.out.printf("\n");
        for (int i = 0; i < 80; i++) {
            System.out.printf("-");
        }
        System.out.printf("\n");
    }
}