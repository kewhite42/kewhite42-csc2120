import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

class AsteroidsGame extends Game implements ActionListener{
    protected Timer timer = new Timer(10, this);  // Allows us to let time pass
    Ship s = new Ship(this.width,this.height);
    ArrayList<MyPolygon> gameObjects = new ArrayList<MyPolygon>();

    public AsteroidsGame() {
        super("Asteroids!",800,600);

        this.frame.addKeyListener(s);

        gameObjects.add(new Asteroid(Asteroid.ROCK.L1, new MyPoint(width/7,height/2), 0, width, height));
        gameObjects.add(new Asteroid(Asteroid.ROCK.L2, new MyPoint(2*width/7,height/2), 0, width, height));
        gameObjects.add(new Asteroid(Asteroid.ROCK.M1, new MyPoint(5*width/7,height/2), 0, width, height));
        gameObjects.add(new Asteroid(Asteroid.ROCK.M2, new MyPoint(6*width/7,height/2), 0, width, height));
        gameObjects.add(new Asteroid(Asteroid.ROCK.S1, new MyPoint(3*width/7,height/2), 0, width, height));
        gameObjects.add(new Asteroid(Asteroid.ROCK.S2, new MyPoint(4*width/7,height/2), 0, width, height));

        timer.start();
    }

    public void paint(Graphics brush) {
        brush.setColor(Color.black);
        brush.fillRect(0,0,this.width,this.height);
        brush.setColor(Color.BLUE);

        for(MyPolygon obj: gameObjects) {
            obj.animate();
            obj.draw(brush);
        }
        s.draw(brush);

        for(MyPolygon obj : gameObjects){
            if(s.collide(obj)){
                s.die();
            }
        }
    }

    public static void main (String[] args) {
        new AsteroidsGame();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        frame.repaint();
    }
}