import javax.swing.*;
import java.awt.*;

abstract class Game extends JPanel {

    protected int width, height;
    protected JFrame frame;
    public Game(String name, int inWidth, int inHeight) {
        width = inWidth;
        height = inHeight;

        frame = new JFrame(name);
        frame.add(this);
        frame.setSize(width,height);
        frame.setVisible(true);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

    // 'paint' will be called every tenth of a second that the game is on.
    abstract public void paint(Graphics brush);

}