import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static java.awt.event.KeyEvent.*;

public class Ship extends MyPolygon implements KeyListener {

    private boolean up = false;
    private boolean left = false;
    private boolean right = false;
    private MyPoint pull;
    private double acceleration = 1.5;
    private boolean dead = false;
    private double rot = 0;  //equivalent of rotation in MyPolygon

    public Ship(int boundX, int boundY) {
        super(getShape(), new MyPoint(boundX / 2, boundY / 2), 0, boundX, boundY);
        pull = new MyPoint(0, 0);
    }

    static private MyPoint[] getShape() {
        MyPoint[] points = new MyPoint[4];
        points[0] = new MyPoint(0, 0);
        points[1] = new MyPoint(-10, 20);
        points[3] = new MyPoint(10, 20);
        points[2] = new MyPoint(0, 15);
        return points;
    }


    @Override
    public void animate() {
        if (!dead) {
            MyPoint p = this.getPosition();
            if (up) {
                accelerate(this.acceleration);  //accelerate only if up is true
                if (getPoints()[0].y < getPoints()[2].y) {
                    p.y--;
                } else {
                    p.y++;
                }
                //wrap around axes
                if (p.y > this.boundY)
                    p.y = -10;
                else if (p.y < 0)
                    p.y = this.boundY + 10;
                if (p.x > this.boundX)
                    p.x = -10;
                else if (p.x < 0)
                    p.x = this.boundX + 10;
            }
            if(left) {
                p.x += pull.x;
                setRotation(this.getRotation());
            }
            else if(right) {
                p.x += pull.x;
                setRotation(this.getRotation());
            }

            this.setPosition(p);
        }
    }

        @Override
        public void draw (Graphics brush){
            if (!dead) {
                Color c = brush.getColor();
                brush.setColor(Color.orange);

                super.draw(brush);
                brush.setColor(c);
            }
        }

    public void keyTyped(KeyEvent e) {

    }

    public void keyPressed(KeyEvent e) {
            switch (e.getKeyCode()) {
               //animate after changing boolean values
                case VK_DOWN:
                case VK_UP: {  //up is true for up or down arrow key
                    up = true;
                    this.animate();
                }
                break;
                case VK_LEFT: {
                    left = true;
                    right = false;
                    this.animate();
                }
                break;
                case VK_RIGHT: {
                    right = true;
                    left = false;
                    this.animate();
                }
        }
    }

    public void keyReleased(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.KEY_RELEASED){  //make sure the key was released, not pressed
            switch (e.getKeyCode()) {
                case VK_DOWN:
                case VK_UP:
                    up = false;
                break;
                case VK_LEFT:
                    left = false;
                    break;
                default:
                    right = false;
            }
        }
    }

    //necessary only when up is true
    public void accelerate(double acceleration) {
        pull.x += (acceleration * Math.cos(Math.toRadians(this.getRotation() - 90)));
        pull.y += (acceleration * Math.cos(Math.toRadians(this.getRotation() - 90)));
    }

    public double getRotation() {
        if (left && !up){
            this.rot -= 25;
            return rot;
        }
        else if (right && !up){
            this.rot += 25;
            return rot;
        }
        return 0;
    }

    //moves ship offscreen & maes ure it isn't animated or drawn
    public void die() {
        dead = true;
        MyPoint p = this.getPosition();
        p.x = 1000;
        p.y = 1000;
        this.setPosition(p);
    }
}