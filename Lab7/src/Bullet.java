import java.awt.*;

public class Bullet extends MyPolygon {
    private MyPoint pull;

    public Bullet(int boundX, int boundY, MyPoint pos, MyPoint dir, double rot){
        super(getShape(), pos, rot, boundX, boundY);
        pull = dir;
    }

    static private MyPoint[] getShape(){
        MyPoint[] points = new MyPoint[3];
        points[0] = new MyPoint(0,0);
        points[1] = new MyPoint(-4, -8);
        points[2] = new MyPoint(4, -8);
        return points;
    }

    @Override
    public void animate() {
            MyPoint p = this.getPosition();
            double rot = this.getRotation();

            accelerate(.9);

            p.x += pull.x;
            p.y += pull.y;

            pull.x *= .95;
            pull.y *= .95;

            this.setPosition(p);
            this.setRotation(rot);
    }

    @Override
    public void draw(Graphics brush) {
            Color c = brush.getColor();
            brush.setColor(Color.yellow);

            super.draw(brush);
            brush.setColor(c);
    }

    public void accelerate (double acceleration) {
        pull.x += (acceleration * Math.cos(Math.toRadians(this.getRotation()-90)));
        pull.y += (acceleration * Math.sin(Math.toRadians(this.getRotation()-90)));
    }
}