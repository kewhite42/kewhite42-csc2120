import java.awt.*;
import java.util.Random;

public class Asteroid extends MyPolygon{
    public enum ROCK {L1, L2, M1, M2, S1, S2};

    Asteroid(ROCK type, MyPoint inPosition, double inRotation, int X, int Y) {
        super(getShape(type),inPosition, inRotation, X, Y);
    }

    @Override
    public void draw(Graphics brush) {
        Color c = brush.getColor();
        brush.setColor(Color.WHITE);

        MyPoint[] currentPoints = getPoints();
        int[] x = new int[currentPoints.length];
        int[] y = new int[currentPoints.length];

        for(int i=0;i<currentPoints.length; i++){
            x[i] = (int)currentPoints[i].x;
            y[i] = (int)currentPoints[i].y;
        }

        Polygon poly = new Polygon(x,y,currentPoints.length);
        brush.drawPolygon(poly);
        brush.setColor(c);
    }

    @Override
    public void animate() {
        //Rap Around on the y axis
        MyPoint p = this.getPosition();

        p.x++;
        p.y++;
        if (p.x > this.boundX + 10){
            p.x = 0 - 10;
        } else if (p.x < -10){
            p.x = boundX+10;
        }

        if(p.y > boundY +10){
            p.y = 0- 10;
        } else if (p.y < -10){
            p.y = boundY+10;
        }

        this.setPosition(p);
    }


    static MyPoint[] getShape(ROCK type){

        MyPoint[] pointArray;
        switch (type) {
            case M1:
                pointArray = new MyPoint[5];
                pointArray[0] = new MyPoint(0,0);
                pointArray[1] = new MyPoint(20,-10);
                pointArray[2] = new MyPoint(25,-15);
                pointArray[3] = new MyPoint(10,-20);
                pointArray[4] = new MyPoint(-10,-10);
                break;
            case M2:
                pointArray = new MyPoint[5];
                pointArray[0] = new MyPoint(0,0);
                pointArray[1] = new MyPoint(10,-5);
                pointArray[2] = new MyPoint(25,-15);
                pointArray[3] = new MyPoint(5,-20);
                pointArray[4] = new MyPoint(-5,-10);
                break;
            case S1:
                pointArray = new MyPoint[5];
                pointArray[0] = new MyPoint(0,0);
                pointArray[1] = new MyPoint(30,-10);
                pointArray[2] = new MyPoint(35,-20);
                pointArray[3] = new MyPoint(20,-30);
                pointArray[4] = new MyPoint(-10,-10);
                break;
            case S2:
                pointArray = new MyPoint[5];
                pointArray[0] = new MyPoint(0,0);
                pointArray[1] = new MyPoint(30,-5);
                pointArray[2] = new MyPoint(35,-10);
                pointArray[3] = new MyPoint(10,-20);
                pointArray[4] = new MyPoint(-10,-10);
                break;
            case L1:
                pointArray = new MyPoint[5];
                pointArray[0] = new MyPoint(0,0);
                pointArray[1] = new MyPoint(30,-10);
                pointArray[2] = new MyPoint(35,-30);
                pointArray[3] = new MyPoint(20,-40);
                pointArray[4] = new MyPoint(-20,-10);
                break;
            case L2:
                pointArray = new MyPoint[6];
                pointArray[0] = new MyPoint(0,0);
                pointArray[1] = new MyPoint(30,-10);
                pointArray[2] = new MyPoint(35,-40);
                pointArray[3] = new MyPoint(20,-40);
                pointArray[4] = new MyPoint(-15,-20);
                pointArray[5] = new MyPoint(-5,-2);
                break;
            default:
                pointArray = new MyPoint[5];
                pointArray[0] = new MyPoint(0,0);
                pointArray[1] = new MyPoint(20,-10);
                pointArray[2] = new MyPoint(25,-15);
                pointArray[3] = new MyPoint(10,-20);
                pointArray[4] = new MyPoint(-10,-10);

                break;
        }
        return pointArray;

    }
}
