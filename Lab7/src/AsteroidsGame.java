import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import static java.awt.event.KeyEvent.VK_SPACE;

class AsteroidsGame extends Game implements ActionListener, KeyListener {
    protected Timer timer = new Timer(10, this);  // Allows us to let time pass
    Random r = new Random(System.currentTimeMillis());

    ArrayList<Asteroid> asteroids = new ArrayList<Asteroid>();
    ArrayList<Bullet> bullets = new ArrayList<Bullet>();

    Ship playerShip;
    private boolean canShoot = true;

    public AsteroidsGame() {
        super("Asteroids!",800,600);

        playerShip = new Ship(this.width,this.height);

        this.frame.addKeyListener(playerShip);
        this.frame.addKeyListener(this);

        //draw 10-20 (random number) asteroids in random places on screen
        int numAsteroids = r.nextInt(10) + 10;
        for(int i = 0; i < numAsteroids; i++){
            Asteroid asteroidToAdd;
            boolean overlap;
            do{
                overlap = false;
                double xPos = this.width * r.nextDouble();
                double yPos = this.height * r.nextDouble();
                int shape = r.nextInt(6);
                switch(shape){
                    case 0: asteroidToAdd = new Asteroid(Asteroid.ROCK.L1, new MyPoint(xPos,yPos), 0, width, height);
                        break;
                    case 1: asteroidToAdd = new Asteroid(Asteroid.ROCK.L2, new MyPoint(xPos,yPos), 0, width, height);
                        break;
                    case 2: asteroidToAdd = new Asteroid(Asteroid.ROCK.M1, new MyPoint(xPos,yPos), 0, width, height);
                        break;
                    case 3: asteroidToAdd = new Asteroid(Asteroid.ROCK.M2, new MyPoint(xPos,yPos), 0, width, height);
                        break;
                    case 4: asteroidToAdd = new Asteroid(Asteroid.ROCK.S1, new MyPoint(xPos,yPos), 0, width, height);
                        break;
                    default: asteroidToAdd = new Asteroid(Asteroid.ROCK.S2, new MyPoint(xPos,yPos), 0, width, height);
                }
                //ensure asteroids do not overlap w/ asteroids or ship starting point
                for(Asteroid a : asteroids){
                    if(a.collision(asteroidToAdd) || playerShip.collision(asteroidToAdd))
                        overlap = true;
                }
            }while(overlap);
            asteroids.add(asteroidToAdd);
        }

        timer.start();
    }

    public void paint(Graphics brush) {
        brush.setColor(Color.black);
        brush.fillRect(0,0,this.width,this.height);
        brush.setColor(Color.BLUE);

        //if ship collides w/ asteroid, remove asteroid from game & ship dies
        //if bullet collides w/ asteroid, remove both from game
        Iterator<Asteroid> asteroidIter = asteroids.iterator();
        while(asteroidIter.hasNext()) {
            Asteroid a = asteroidIter.next();

            if(playerShip.collision(a)){
                asteroidIter.remove();
                playerShip.die();
            }
            Iterator<Bullet> bulletIter = bullets.iterator();
            while(bulletIter.hasNext()){
                Bullet b = bulletIter.next();
                if(b.collision(a)){
                    asteroidIter.remove();
                    bulletIter.remove();
                }
            }
        }

        //if ship collides w/ bullet, remove bullet & ship dies
        //if bullet offscreen, remove bullet
        Iterator<Bullet> bulletIter = bullets.iterator();
        while(bulletIter.hasNext()){
            Bullet b = bulletIter.next();

            if(b.getPosition().x < 0 || b.getPosition().x > this.width || b.getPosition().y < 0 || b.getPosition().y > this.height)
                bulletIter.remove();
        }

        //animate & draw Asteroids
        for(Asteroid asteroid : asteroids){
            asteroid.animate();
            asteroid.draw(brush);
        }

        //animate & draw Bullets
        for(Bullet bullet: bullets){
            bullet.animate();
            bullet.draw(brush);
        }

        //animate & draw Ship
        playerShip.animate();
        playerShip.draw(brush);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    //shoot when space bar pressed
    public void keyPressed(KeyEvent e) {
            if(this.canShoot && e.getKeyCode() == VK_SPACE) {
                Bullet b = playerShip.shoot();

                if(b != null){
                    this.bullets.add(b);
                    this.canShoot = false;
                }
            }
    }

    @Override
    //don't shoot once the space bar is released
    public void keyReleased(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_SPACE)
            this.canShoot = true;
    }

    public static void main (String[] args) {
        new AsteroidsGame();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        frame.repaint();
    }
}