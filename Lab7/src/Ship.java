import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Ship extends MyPolygon implements KeyListener{

    private boolean move_left = false;
    private boolean move_right = false;
    private boolean move_up = false;
    //private boolean move_down = false;

    private boolean alive = true;
    private MyPoint pull = new MyPoint(0,0);


    public Ship(int boundX, int boundY){
        super(getShape(), new MyPoint(boundX/2,boundY/2),0, boundX,boundY);
    }

    static private MyPoint[] getShape(){
        MyPoint[] points = new MyPoint[4];
        points[0] = new MyPoint(0,0);
        points[1] = new MyPoint(-10, 20);
        points[3] = new MyPoint(10, 20);
        points[2] = new MyPoint(0, 15);
        return points;
    }

    public void die(){
        alive = false;
    }

    public void accelerate (double acceleration) {
        pull.x += (acceleration * Math.cos(Math.toRadians(this.getRotation()-90)));
        pull.y += (acceleration * Math.sin(Math.toRadians(this.getRotation()-90)));
    }


    @Override
    public void animate() {
        if(alive) {
            MyPoint p = this.getPosition();
            double rot = this.getRotation();
            if (move_left) {
               rot-= 2;
            }
            if (move_right) {
                rot+= 2;
            }
            if (move_up) {
                accelerate(.8);
            }
            /*if (move_down) {
                //accelerate(.5);
            }*/

            p.x += pull.x;
            p.y += pull.y;

            pull.x *= .9;
            pull.y *= .9;

            if (p.x > this.boundX + 10) {
                p.x = 0 - 10;
            } else if (p.x < -10) {
                p.x = boundX + 10;
            }

            if (p.y > boundY + 10) {
                p.y = 0 - 10;
            } else if (p.y < -10) {
                p.y = boundY + 10;
            }

            this.setPosition(p);
            this.setRotation(rot);
        }
    }

    @Override
    public void draw(Graphics brush) {
        if(alive) {
            Color c = brush.getColor();
            brush.setColor(Color.orange);

            super.draw(brush);
            brush.setColor(c);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

        switch (e.getKeyCode()){
            case KeyEvent.VK_LEFT:
                move_left =true;
                break;
            case KeyEvent.VK_RIGHT:
                move_right=true;
                break;
            case KeyEvent.VK_UP:
                move_up = true;
                break;
            //case KeyEvent.VK_DOWN:
                //move_down = true;
            default:
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()){
            case KeyEvent.VK_LEFT:
                move_left =false;
                break;
            case KeyEvent.VK_RIGHT:
                move_right=false;
                break;
            case KeyEvent.VK_UP:
                move_up = false;
                break;
            //case KeyEvent.VK_DOWN:
              //  move_down = false;
            default:
                break;
        }
    }

    public Bullet shoot(){
        if(alive) {
            MyPoint bulletPos = this.getPosition();
            //MyPoint bulletPull = this.pull;
            //double rot = this.getRotation();
            //MyPoint bulletPull = new M;//new MyPoint(this.boundX/100, this.boundY/100);

            return new Bullet(this.boundX, this.boundY, bulletPos, this.pull, this.getRotation());
        }
        else
            return null;
    }
}
