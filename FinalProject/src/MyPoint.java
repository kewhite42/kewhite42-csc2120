/**
 * This class encapsulates x- and y- coordinates for easier manipulation.
 *
 * @author Ania Kaczka Jennings
 */
public class MyPoint {
    /**
     * The doubles that represent this MyPoint's x- and y-coordinates, respectively
     */
    double x,y;

    /**
     * Creates this MyPoint from coordinates.
     *
     * @param inX The x-coordinate
     * @param inY The y-coordinate
     */
    public MyPoint(double inX, double inY) { x = inX; y = inY; }

}
