import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 * This class creates, draws, and animates Bullets.
 *
 * @author Ania Kaczka Jennings
 * @author Kaley White
 */
public class Bullet extends MyCircle {
    /**
     * True if a letter has been displayed for this
     */
    private boolean letterFired = false;

    /**
     * Creates this Bullet of given position, rotation, and size.<br>
     * Calls the MyCircle constructor.
     *
     * @param position The location of this Bullet on the screen
     * @param rotation The orientation of this Bullet on the screen
     * @param radius The radius of the MyCircle that defines this Bullet
     */
    Bullet(MyPoint position, double rotation, double radius){
        super(position,rotation,radius);
        this.accelerate(.995);
    }

    /**
     * Getter for letterFired
     *
     * @return boolean Value of this.letterFired
     */
    public boolean getLetterFired(){
        return letterFired;
    }

    /**
     * Setter for letterFired
     *
     * @param letterFired New value for this.letterFired
     */
    public void setLetterFired(boolean letterFired){
        this.letterFired = letterFired;
    }

    /**
     * Moves this Bullet based on position, rotation, and acceleration.<br>
     */
    @Override
    public void animate() {
        MyPoint tmp = this.getPosition();
        tmp.x += pull.x;
        tmp.y += pull.y;
        this.setPosition(tmp);
    }

    /**
     * Draws and fills this Bullet as a MyCircle on the screen.<br>
     * When finished drawing, changes brush color back to what it was before.
     *
     * @param brush So Graphics can be used
     */
    @Override
    public void draw(Graphics brush) {
        Color c = brush.getColor();
        brush.setColor(Color.RED);
        super.draw(brush);
        brush.setColor(c);
    }

    /**
     * For all intents and purposes, this Bullet does not collide with any
     * object that will be passed to this function.
     *
     * @param other The MyShape object that this Bullet theoretically could collide with
     */
    @Override
    public boolean collision(MyShape other) {
        return false;
    }


}