import java.awt.*;

/**
 * Gives more points per asteroid hit, for a specific duration
 *
 * @author Kaley White
 */
public class ScoreMult extends Powerup {

    /**
     * Creates this in position specified by randPt
     *
     * @param randPt Position in which to draw this on the screen
     * @param myShip Ship that can collect this
     */
    public ScoreMult(MyPoint randPt, Ship myShip){
        super(randPt, myShip);
        type = "score mult";
        color = Color.white;
        dist = 0;
    }

    /**
     * Allows ship member to collect this
     */
    public void collect(){
        ship.setHasPowerup(true);
        ship.setHasScoreMult(true);
    }
}
