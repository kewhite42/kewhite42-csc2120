import java.awt.*;

/**
 * Visual representation of the lives ship has left
 *
 * @author Kaley White
 */
public class LivesShip extends MyPolygon {
    /**
     * The number LivesShip this is in the row
     */
    private int numLife;

    /**
     * Creates this LivesShip
     *
     * @param numLife The number LivesShip this is in the row.
     *                Determines this LivesShip's position on the screen.
     * @param inBoundX The width of the game screen
     * @param inBoundY The height of the game screen
     */
    LivesShip(int numLife, int inBoundX, int inBoundY){
        super(Ship.getShape(), getLocation(numLife), 0, inBoundX, inBoundY);
        this.numLife = numLife;
    }

    /**
     * Gets position of this LivesShip on the screen
     *
     * @param numLife The number of LivesShips from the "Lives" label this LivesShips is
     * @return MyPoint The (x,y) point that defines this LivesShip's position on the screen
     */
    private static MyPoint getLocation(int numLife){
        double startLoc = 80;
        for(int i = 0; i < numLife; i++)
            startLoc += 20;
        return new MyPoint(startLoc, 10);
    }

    /**
     * Draws and fills in this LivesShip.<br>
     * numLife determines the color.<br>
     * When finished drawing, resets color to what it was before.
     *
     * @param brush So Graphics can be used
     */
    @Override
    public void draw(Graphics brush) {
        Color c = brush.getColor();
        if(numLife <= 3)
            brush.setColor(Color.red);
        else if(numLife <= 7)
            brush.setColor(Color.yellow);
        else
            brush.setColor(Color.green);
        super.draw(brush);
        brush.setColor(c);
    }

    /**
     * LivesShip does not move.
     */
    public void animate(){

    }
}
