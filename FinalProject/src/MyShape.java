import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 * This class allows its subclasses to be created, drawn, animated,
 * and checked for collisions.
 *
 * @author Ania Kaczka Jennings
 */
public abstract class MyShape {
    /**
     * Represents the orientation of this MyShape on the screen.<br>
     * Zero degrees is due east.
     */
    private double rotation;

    /**
     * The (x,y) point that is this MyShape's location ("offset") on the screen.
     */
    private MyPoint position;

    /**
     * The (x,y) coordinates of this MyShape's "velocity"
     */
    protected MyPoint pull = new MyPoint(0,0);

    /**
     * Moves this MyShape on the screen.
     */
    abstract void animate();

    /**
     * Draws this MyShape on the screen.
     *
     * @param brush So Graphics can be used
     */
    abstract void draw(Graphics brush);

    /**
     * Gets the bounds of this MyShape
     *
     * @return Rectangle2D The rectangle whose dimensions define
     * the bounds of this MyShape
     */
    abstract Rectangle2D getBounds();

    /**
     * Accelerates/decelerates this MyShape.<br>
     * Rotation remains the same.
     *
     * @param acceleration The current acceleration of this MyShape
     */
    protected void accelerate (double acceleration) {
        pull.x += (acceleration * Math.cos(Math.toRadians(this.getRotation()-90)));
        pull.y += (acceleration * Math.sin(Math.toRadians(this.getRotation()-90)));
    }

    /**
     * Creates this MyShape of given position and rotation.<br>
     * Is not used outside of subclasses, so coordinates that
     * define this MyShape's outline are provided in subclasses.
     *
     * @param position The location of this MyShape on the screen
     * @param rotation The orientation of this myShape on the screen
     */
    public MyShape(MyPoint position, double rotation){
        this.rotation = rotation;
        this.position = position;
    }

    /**
     * Gets the rotation of this MyShape
     *
     * @return double The rotation of this MyShape
     */
    protected double getRotation(){return rotation;}

    /**
     * Sets the rotation of this MyShape.<br>
     * Ensures that rotation is between 0 and 360, inclusive
     *
     * @param rot The new rotation of this MyShape
     */
    protected void setRotation(double rot){
        if(rot < 0){
            rot = rot + 360;
        }
        this.rotation = rot % 360;
    }

    /**
     * Checks if this MyShape overlaps the parameter MyShape
     *
     * @param other The MyShape to be checked for collision with this MyShape
     * @return boolean True if the MyShapes overlap, false otherwise
     */
    public boolean collision(MyShape other){

        Rectangle2D thisBounds = this.getBounds();
        Rectangle2D otherBounds = other.getBounds();

        return thisBounds.intersects(otherBounds);
    }

    /**
     * Gets the position of this MyShape
     *
     * @return MyPoint The (x,y) point that defines this MyShape's position
     */
    protected MyPoint getPosition(){
        return new MyPoint(position.x,position.y);
    }

    /**
     * Sets this MyShape's position
     *
     * @param p The (x,y) point that defines the new position for this MyShape
     */
    protected void setPosition(MyPoint p){
        position = p;
    }

}
