import java.awt.*;

/**
 * ExtraLife Powerups give ship an extra life
 *
 * @author Kaley White
 */
public class ExtraLife extends Powerup {

    /**
     * Creates this in position specified by randPt
     *
     * @param randPt Position in which to draw this on the screen
     * @param myShip Ship that can collect this
     */
    public ExtraLife(MyPoint randPt, Ship myShip){
        super(randPt, myShip);
        type = "extra life";
        color = Color.BLUE;
    }

    /**
     * Allows ship member to collect this<br>
     * No ship.setHasExtraLife() b/c no limit to ExtraLife duration, unlike other Powerups
     */
    public void collect(){
        ship.setHasPowerup(true);
    }
}
