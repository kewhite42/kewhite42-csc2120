import java.awt.*;

/**
 * Gives ship special "boosters"
 *
 * @author Kaley White
 */
abstract class Powerup extends MyPolygon{

    /**
     * Ship to be updated when it collides witn this
     */
    protected Ship ship;

    /**
     * String ID of this
     */
    protected String type;

    /**
     * RGB color of this
     */
    protected Color color;

    /**
     * Distance ship has traveled with this (in pixels)
     */
    protected int dist;

    /**
     * Ctreates this Powerup in position specified by randPt
     *
     * @param randPt (x,y) position in which to draw this on the screen
     * @param myShip Ship that can interact with this
     */
    public Powerup(MyPoint randPt, Ship myShip){
        super(getShape(), randPt, 0, 800, 600);
        ship = myShip;
    }

    /**
     * Getter for dist
     *
     * @return int Value of dist
     */
    public int getDist(){
        return dist;
    }

    /**
     * Increments dist by 1
     */
    public void incrementDist(){
        dist++;
    }

    /**
     * Getter for type
     *
     * @return String Value of type
     */
    public String getType(){
        return type;
    }

    /**
     * Gets shape that defines this<br>
     * Is not coordinates of shape on screen
     *
     * @return MyPoint[] The (x,y) coordinates that define this
     */
    private static MyPoint[] getShape(){
        MyPoint[] shape = new MyPoint[10];
        shape[0] = new MyPoint(0,0);
        shape[1] = new MyPoint(10,-5);
        shape[2] = new MyPoint(5,5);
        shape[3] = new MyPoint(10,10);
        shape[4] = new MyPoint(5,10);
        shape[5] = new MyPoint(0,20);
        shape[6] = new MyPoint(-5,10);
        shape[7] = new MyPoint(-10,10);
        shape[8] = new MyPoint(-5,5);
        shape[9] = new MyPoint(-10,-5);
        return shape;
    }

    /**
     * Allows ship member to collect this
     */
    public abstract void collect();


    /**
     * Fills this in in this.color.<br>
     * When finished drawing, changes the color of the brush to what it was before.
     *
     * @param brush For Graphics
     */
    public void draw(Graphics brush){
        Color c = brush.getColor();
        brush.setColor(color);
        super.draw(brush);
        brush.setColor(c);
    }

    /**
     * Powerups do not move
     */
    public void animate(){

    }
}
