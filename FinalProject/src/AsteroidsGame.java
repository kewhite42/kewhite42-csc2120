import javax.imageio.ImageIO;
import javax.sound.sampled.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.*;
import static java.awt.Color.white;

/**
 * This class creates a new AsteroidsGame that responds to keyboard input
 * and redraws the screen at intervals.
 *
 * @author Ania Kaczka Jennings
 * @author Kaley White
 */
class AsteroidsGame extends Game implements ActionListener, KeyListener {
    /**
     * Possible state this AsteroidsGame is in
     */
    public enum State {START, GAME, GAME_OVER, HIGH_SCORES, WIN}

    /**
     *  The state this AsteroidsGame is in
     */
    private State state = State.START;

    /**
     * Timer that allows this to be redrawn every 10 ms
     */
    private Timer timer = new Timer(10, this);

    /**
     * True if the player can shoot, false otherwise
     */
    private boolean can_fire_shot = true;

    /**
     * Number of lives the player has left
     */
    private int lives = 10;

    /**
     * The Ship that the player controls to play this game
     */
    private Ship playerShip;

    /**
     * Keeps track of this player's score
     */
    private int score = 0;

    /**
     * Allows randomness when Asteroids are spawned
     */
    private Random rand;

    /**
     * Letters shown when ship fires
     * Read from a file
     */
    private String bulletWords;

    /**
     * Index of next bulletWords character to display when ship fires
     */
    private int posInBulletWords = 0;

    /**
     * True if letters are displayed when ship fires, false otherwise
     */
    private boolean fireLetters = true;

    /**
     * Number of <Integer, ArrayList<String>>></String> pairs scoresPlayers TreeMap
     */
    private int highScoresLines = 0;

    /**
     * List of Asteroids on this screen
     */
    private ArrayList<Asteroid> asteroids = new ArrayList<>();

    /**
     * List of Bullets on this screen
     */
    private ArrayList<Bullet> bullets = new ArrayList<>();

    /**
     * List of powerups to be drawn on this screen
     */
    private ArrayList<Powerup> powerups_to_draw = new ArrayList<>();

    /**
     * List of powerups that ship is currently using
     */
    private ArrayList<Powerup> powerups_not_drawn = new ArrayList<>();

    /**
     * Holds as many shipShape Polygons as player has lives
     */
    private LinkedList<LivesShip> extraShips = new LinkedList<>();

    /**
     * Sorted list of scores and corresponding players
     * Read in from file
     */
    private TreeMap<Integer, ArrayList<String>> scoresPlayers = new TreeMap<>();

    /**
     * Location of collide sound file<br>
     * collide plays when ship collides w/ asteroid
     */
    private URL collideLoc;

    /**
     * Gets audio from collideLoc
     */
    private AudioInputStream getCollide;

    /**
     * Sound clip for collide
     */
    private Clip collide;

    /**
     * Location of hit sound file<br>
     * hit plays when a bullet hits an asteroid
     */
    private URL hitLoc;

    /**
     * Gets audio from hitLoc
     */
    private AudioInputStream getHit;

    /**
     * Sound clip for hit
     */
    private Clip hit;

    /**
     * Location of shot sound file<br>
     * shot plays when ship shoots
     */
    private URL shotLoc;

    /**
     * Gets audio from shotLoc
     */
    private AudioInputStream getShot;

    /**
     * Sound clip for shot
     */
    private Clip shot;

    /**
     * Location of backgroundClip sound file<br>
     * Background music for game
     */
    private URL backgroundLoc;

    /**
     * Gets audio from backgroundLoc
     */
    private AudioInputStream getBackground;

    /**
     * Sound clip for backgroundClip
     */
    private Clip backgroundClip;

    /**
     * Location of powerup sound file<br>
     * powerup plays when ship collects a powerup
     */
    private URL powerupLoc;

    /**
     * Location of powerup sound file
     */
    private AudioInputStream getPowerup;

    /**
     * Sound clip for powerup
     */
    private Clip powerup;

    /**
     * Creates this AsteroidsGame.<br>
     * Calls the Game constructor.<br>
     * Creates playerShip, spawns Asteroids, and starts the timer.
     */
    public AsteroidsGame(){
        super("Asteroids!", 800, 600);
        rand = new Random();
        rand.setSeed(System.currentTimeMillis());

        playerShip = new Ship(this.width, this.height);
        this.frame.addKeyListener(playerShip);
        this.frame.addKeyListener(this);

        BufferedReader br;
        try{
            br = new BufferedReader(new FileReader("Bullet_Words.txt"));
            bulletWords = br.readLine();

        }catch(IOException e){
            fireLetters = false;
        }

        populate();

        timer.start();
    }

    /**
     * Populates powerups_to_draw, extraShips, and asteroids
     */
    private void populate(){
        //populate powerups_to_draw
        for(int i = 0; i < 3; i++){
            int powerupType = rand.nextInt(5);
            double xPt = rand.nextDouble()*800;
            double yPt = rand.nextDouble()*600;
            MyPoint pos = new MyPoint(xPt, yPt);
            Powerup p = null;
            switch(powerupType){
                case 0: p = new ExtraLife(pos, playerShip);
                    break;
                case 1: p = new ScoreMult(pos, playerShip);
                    break;
                case 2: p = new Invincibility(pos, playerShip);
                    break;
                case 3: p = new BigBullet(pos, playerShip);
                    break;
                case 4: p = new InvincBullet(pos, playerShip);
                    break;
            }
            powerups_to_draw.add(p);
        }

        //populate extraShips
        for(int i = 1; i <= lives; i++){
            LivesShip ls = new LivesShip(i, width, height);
            extraShips.add(ls);
        }

        //populate asteroids
        int numAst = rand.nextInt(30)+20;
        spawnAsteroids(numAst);
    }

    /**
     * Populates this screen with the given number of Asteroids.<br>
     * Asteroids are of random type and random position on this screen.
     */
    private void spawnAsteroids(int count) {
        for (int i = 0; i < count; i++) {
            int val = rand.nextInt(Asteroid.ROCK.values().length);
            Asteroid.ROCK type = Asteroid.ROCK.values()[val];
            int x = rand.nextInt(width);
            int y = rand.nextInt(height);

            if (x < width / 2 + 100 && x > width / 2 - 100) {
                x += 200;
            }
            if (y < height / 2 + 100 && y > height / 2 - 100) {
                y += 200;
            }


            asteroids.add(new Asteroid(false, type, new MyPoint(x, y), rand.nextInt() % 360, width, height));
        }
    }

    /**
     * Reads scores from file and puts into Treemap of "score, players with that score"
     *
     * @return String The exact String of players and scores to be drawn on this screen
     */
    private String getHighScores(){
        String ret = "";
        try{
            BufferedReader br = new BufferedReader(new FileReader("High_Scores.txt"));
            while(br.readLine() != null) {
                String line = br.readLine();
                String[] splitLine = line.split(" ");
                if (!scoresPlayers.containsKey(Integer.parseInt(splitLine[0]))) {
                    ArrayList<String> listToAdd = new ArrayList<>();
                    listToAdd.add(splitLine[1]);
                    scoresPlayers.put(Integer.parseInt(splitLine[0]), listToAdd);
                } else {
                    scoresPlayers.get(Integer.parseInt(splitLine[0])).add(splitLine[1]);
                }
                Set entries = scoresPlayers.entrySet();
                for (Map.Entry<Integer, ArrayList<String>> entry : scoresPlayers.entrySet()) {
                    Integer score = entry.getKey();
                    ArrayList<String> players = entry.getValue();
                    for (String p : players) {
                        ret = ret + "\n" + p + " " + score;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "Error!";
        }
        highScoresLines = scoresPlayers.size();
        return ret;
    }

    /**
     * Removes a life if game is not ended, changes state if game needs to end
     * Writes score to file if game has ended
     *
     * @param win True if game has ended by player's winning, false otherwise
     * @return boolean True if write was successful, false otherwise
     */
    private boolean removeLife(boolean win){
        //if player has more lives but hasn't won
        if(extraShips.size() > 0 && !win) {
            extraShips.removeLast();
            lives--;
        }
        //player is out of lives or has won
        if(lives == 0 || win) {
            if(lives == 0){
                playerShip.die();
                state = State.GAME_OVER;
                backgroundClip.stop();
            }
            if(win) {
                state = State.WIN;
                backgroundClip.stop();
            }
            playerShip = new Ship(800, 600);

            //write to high scores file
            try{
                PrintWriter pw = new PrintWriter(new FileOutputStream("High_Scores.txt", true));
                LocalDateTime player = LocalDateTime.now();
                pw.println(score + " " + player.getMonthValue() + "/" + player.getDayOfMonth() + "/" + player.getYear());
                pw.flush();
            } catch (FileNotFoundException e) {
                return false;
            }
        }
        return true;
    }

    /**
     * Draws playerShip, Asteroids, Bullets, LivesShips, and Powerups on this screen.<br>
     * Before drawing, removes objects that collide and a LivesShip if playerShip was part of that collision,
     * and Powerups that player has collected. Performs effects when collision occurs.
     *
     * @param brush So Graphics can be used
     */
    public void paint(Graphics brush) {
        if (state == State.START) {
            brush.setColor(Color.yellow);
            brush.fillRect(0, 0, 800, 600);
            brush.setFont(new Font("f", Font.BOLD, 48));
            brush.setColor(Color.black);
            brush.drawString("Press space to start", 100, 275);
            brush.setColor(Color.BLUE);
        } else if (state == State.GAME_OVER) {
            brush.setColor(Color.red);
            brush.fillRect(0, 0, 800, 600);
            brush.setFont(new Font("f", Font.BOLD, 48));
            brush.setColor(Color.black);
            brush.drawString("Game over!", 100, 275);
            brush.drawString("Press space to play again", 100, 325);
            brush.drawString("Press h to see high scores", 100, 375);
            brush.setColor(Color.BLUE);
        } else if (state == State.HIGH_SCORES) {
            brush.setColor(white);
            brush.fillRect(0, 0, 800, 600);
            brush.setFont(new Font("f", Font.BOLD, 35));
            brush.setColor(Color.black);
            brush.drawString("Player: ", 25, 25);
            brush.drawString("Score: ", 350, 25);
            brush.setFont(new Font("g", Font.PLAIN, 30));
            brush.drawString(getHighScores(), 25, 90);
            brush.drawString("Press space to play again", 25, 150*highScoresLines);
        } else if (state == State.WIN) {
            brush.setColor(Color.green);
            brush.fillRect(0, 0, 800, 600);
            brush.setFont(new Font("f", Font.BOLD, 48));
            brush.setColor(Color.black);
            brush.drawString("YOU WIN!", 100, 275);
            brush.drawString("Press space to play again", 100, 325);
            brush.drawString("Press h to see high scores", 100, 375);
            brush.setColor(Color.BLUE);
        } else {
            try{
                BufferedImage backgroundImg = ImageIO.read(new File("C:\\Users\\kwhit\\Desktop\\CSC Stuff\\OO\\git\\FinalProject\\asteroids_background.jpg"));
                brush.drawImage(backgroundImg, 0, 0, 800, 600, null);
            }catch(IOException e){
                brush.setColor(Color.black);
                brush.fillRect(0, 0, 800, 600);
                brush.setColor(Color.BLUE);
            }

            if (asteroids.isEmpty()) {
                state = State.WIN;
                removeLife(false);
            }

            for (Asteroid obj : asteroids) {
                obj.animate();
                obj.draw(brush);
                if (playerShip.collision(obj) && playerShip.isAlive()) {
                    if(!playerShip.getHasInvincibility()) {
                        //play collide sound
                        try {
                            collideLoc = this.getClass().getClassLoader().getResource("asteroids_collide.wav");
                            getCollide = AudioSystem.getAudioInputStream(collideLoc);
                            collide = AudioSystem.getClip();
                            collide.open(getCollide);
                            collide.start();
                        } catch (UnsupportedAudioFileException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (LineUnavailableException e) {
                            e.printStackTrace();
                        }
                        if (!obj.getHasCollided()) {
                            obj.setHasCollided(true);
                            boolean removeLifeSuccess = removeLife(false);
                            if (!removeLifeSuccess) {
                                brush.setColor(white);
                                brush.drawString("Could not write high score to file.", 200, 500);
                            }
                        }
                    }
                }
            }

            //if player has run into every asteroid, pity party and player wins
            boolean win = true;
            for(Asteroid a : asteroids){
                if(!a.getHasCollided())
                    win = false;
            }
            if(win) {
                state = State.WIN;
                removeLife(true);
            }

            //powerups are in powerups_to_draw until collected by ship
            //when collected, added to powerups_not_drawn
            //when powerup.getDist() > 10000, powerup is added to powerups_to_remove
            ArrayList<Powerup> powerups_to_remove = new ArrayList<>();
            for (Powerup p : powerups_to_draw) {
                p.animate();
                p.draw(brush);
                if (playerShip.collision(p)) {
                    p.collect();
                    //play powerup sound
                    try {
                        powerupLoc = this.getClass().getClassLoader().getResource("asteroids_powerup.wav");
                        getPowerup = AudioSystem.getAudioInputStream(powerupLoc);
                        powerup = AudioSystem.getClip();
                        powerup.open(getPowerup);
                        powerup.start();
                    } catch (UnsupportedAudioFileException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (LineUnavailableException e) {
                        e.printStackTrace();
                    }
                    powerups_not_drawn.add(p);
                    if (p.type.equals("extra life")){
                        playerShip.drawWithPowerup(brush);
                        if(lives != 10) {
                            LivesShip ls = new LivesShip(lives + 1, 800, 600);
                            extraShips.add(ls);
                        }
                        powerups_to_remove.add(p);
                    }
                }
            }

            ArrayList<Bullet> bullets_to_remove = new ArrayList<>();
            ArrayList<Asteroid> ast_for_effect = new ArrayList<>();
            ArrayList<Asteroid> ast_to_remove = new ArrayList<>();
            for (Bullet b : bullets) {
                b.animate();
                b.draw(brush);
                if(fireLetters){
                    if(!b.getLetterFired()){
                        brush.setColor(Color.white);
                        brush.setFont(new Font("h", Font.BOLD, 50));
                        MyPoint pos = b.getPosition();
                        brush.drawString(bulletWords.substring(posInBulletWords, posInBulletWords+1), (int)pos.x, (int)pos.y);
                        b.setLetterFired(true);
                        posInBulletWords++;
                        if(posInBulletWords == bulletWords.length())
                            posInBulletWords = 0;
                    }
                }
                if (b.getPosition().x > width || b.getPosition().y > height || b.getPosition().x < 0 || b.getPosition().y < 0)
                    bullets_to_remove.add(b);

                for (Asteroid obj : asteroids) {
                    if (obj.collision(b)) {
                        //play hit sound
                        try {
                            hitLoc = this.getClass().getClassLoader().getResource("asteroids_hit.wav");
                            getHit = AudioSystem.getAudioInputStream(hitLoc);
                            hit = AudioSystem.getClip();
                            hit.open(getHit);
                            hit.start();
                        } catch (UnsupportedAudioFileException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (LineUnavailableException e) {
                            e.printStackTrace();
                        }
                        if (!playerShip.getHasInvincBullet())
                            bullets_to_remove.add(b);
                        ast_for_effect.add(obj);
                        if (playerShip.isAlive()) {
                            if (playerShip.getHasScoreMult())
                                score += 20;  //player gets 20 points for hitting an asteroid if has ScoreMult powerup
                            else
                                score += 10;  //10 points otherwise
                        }
                    }
                }
                if(score == 100)
                    spawnAsteroids(10);
                else if(score == 200)
                    spawnAsteroids(20);
                else if(score == 500)
                    spawnAsteroids(50);
                else if(score == 1000)
                    spawnAsteroids(100);
            }

            bullets.removeAll(bullets_to_remove);

            for(Asteroid a : ast_for_effect){
                int effect = rand.nextInt(2);
                //change asteroid color before removing
                if(effect == 0){
                    for(int i = 0; i < 100000; i++)
                        a.drawWithEffect(brush);
                    ast_to_remove.add(a);
                }
                //split asteroid when shot
                else{
                    ArrayList<Asteroid> splitAsteroids = a.split();
                    ast_to_remove.add(a);
                    asteroids.addAll(splitAsteroids);
                }
            }

            asteroids.removeAll(ast_to_remove);
            powerups_to_draw.removeAll(powerups_not_drawn);

            //playerShip.animate();

            for (Powerup powerup : powerups_not_drawn) {
                powerup.incrementDist();
                if (powerup.getDist() > 150) {
                    powerups_to_remove.add(powerup);
                    switch(powerup.type){
                        case "score mult": playerShip.setHasScoreMult(false);
                            break;
                        case "big bullet": playerShip.setHasBigBullet(false);
                            break;
                        case "invinc bullet": playerShip.setHasInvincBullet(false);
                            break;
                        case "invincibility": playerShip.setHasInvincibility(false);
                            break;
                        default:
                            break;
                    }
                }
            }

            powerups_not_drawn.removeAll(powerups_to_remove);

            if (powerups_not_drawn.isEmpty())
                playerShip.setHasPowerup(false);

            playerShip.animate();
            if (playerShip.getHasPowerup())
                playerShip.drawWithPowerup(brush);
            else
                playerShip.draw(brush);

            brush.setColor(white);
            brush.setFont(new Font("f", Font.PLAIN, 28));
            if (lives == 0)
                brush.drawString("Lives: 0", 10, 25);
            else
                brush.drawString("Lives: ", 10, 25);
            brush.drawString("Score: " + score, width - 150, 25);

            for (LivesShip ls : extraShips)
                ls.draw(brush);
        }
    }


    /**
     * Resets this so player can play again
     */
    private void resetMembers(){
        timer.stop();
        can_fire_shot = true;
        lives = 10;
        score = 0;
        state = State.GAME;

        ArrayList<Asteroid> ast_to_remove = new ArrayList<>();
        ast_to_remove.addAll(asteroids);
        asteroids.removeAll(ast_to_remove);
        int numAst = rand.nextInt(30)+20;
        spawnAsteroids(numAst);

        bullets.clear();
        powerups_to_draw.clear();
        powerups_not_drawn.clear();

        backgroundLoc = null;
        getBackground = null;
        backgroundClip = null;
        hitLoc = null;
        getHit = null;
        hit = null;
        shotLoc = null;
        getShot = null;
        shot = null;
        collideLoc = null;
        getCollide = null;
        collide = null;
        powerupLoc = null;
        getPowerup = null;
        powerup = null;

        populate();
        timer.start();
    }

    /**
     * Calls this AsteroidsGame constructor.
     *
     * @param args Unused
     */
    public static void main(String[] args) {
        new AsteroidsGame();
    }

    /**
     * Updates the game screen.
     *
     * @param e So ActionEvent can be used
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        frame.repaint();
    }

    /**
     * No action is taken when a key is typed.
     *
     * @param e So KeyEvent can be used
     */
    @Override
    public void keyTyped(KeyEvent e) {

    }

    /**
     * Handles space key input based on state
     *
     * @param e So KeyEvent can be used
     */
    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_SPACE:
                if (state == State.START) {
                    state = State.GAME;
                    //play background music
                    try {
                        backgroundLoc = this.getClass().getClassLoader().getResource("asteroids_background_music.wav");
                        getBackground = AudioSystem.getAudioInputStream(backgroundLoc);
                        backgroundClip = AudioSystem.getClip();
                        backgroundClip.open(getBackground);
                        backgroundClip.loop(Clip.LOOP_CONTINUOUSLY);
                        backgroundClip.start();
                    } catch (UnsupportedAudioFileException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (LineUnavailableException ex) {
                        ex.printStackTrace();
                    }
                } else if(state == State.GAME_OVER || state == State.HIGH_SCORES || state == State.WIN){
                    resetMembers();
                    //play background music
                    try {
                        backgroundLoc = this.getClass().getClassLoader().getResource("asteroids_background.wav");
                        getBackground = AudioSystem.getAudioInputStream(backgroundLoc);
                        backgroundClip = AudioSystem.getClip();
                        backgroundClip.open(getBackground);
                        backgroundClip.loop(Clip.LOOP_CONTINUOUSLY);
                        backgroundClip.start();
                    } catch (UnsupportedAudioFileException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (LineUnavailableException ex) {
                        ex.printStackTrace();
                    }
                } else{
                    if(can_fire_shot){
                        Bullet b = playerShip.shoot();
                        //play shot sound
                        try {
                            shotLoc = this.getClass().getClassLoader().getResource("asteroids_shot.wav");
                            getShot = AudioSystem.getAudioInputStream(shotLoc);
                            shot = AudioSystem.getClip();
                            shot.open(getShot);
                            shot.start();
                        } catch (UnsupportedAudioFileException ex) {
                            ex.printStackTrace();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        } catch (LineUnavailableException ex) {
                            ex.printStackTrace();
                        }
                        if (b != null) {
                            bullets.add(b);
                        }
                        can_fire_shot = false;
                    }
                }
            break;
            case KeyEvent.VK_H:
                if(state == State.GAME_OVER || state == State.WIN) {
                    state = State.HIGH_SCORES;
                    backgroundClip.stop();
                }
            break;
        }
    }

    /**
     * Handles release of space key based on state
     *
     * @param e So KeyEvent can be used
     */
    @Override
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_SPACE:
                if(state == State.GAME)
                    can_fire_shot = true;
                break;
        }
    }
}