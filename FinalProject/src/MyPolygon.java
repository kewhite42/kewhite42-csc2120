import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 * This class creates, draws, and calculates stats (e.g. area)
 * for MyPolygon objects.
 *
 * @author Ania Kaczka Jennings
 */
abstract public class MyPolygon extends MyShape {

    /**
     * The array of (x,y) points that are the vertices of this MyPolygon
     */
    private MyPoint[] shape;

    /**
     * The int width and height of this screen, respectively
     */
    protected int boundX,boundY;

    /**
     * Creates a MyPolygon object of specified shape, position, and orientation. Adjusts for
     * origin being in top-left corner.
     *
     * @param inShape The array of (x,y) points that define the shape
     * @param inPosition The location of the shape on the screen
     * @param inRotation The orientation of the shape (which direction it's "pointing")
     * @param boundX The width of the game screen
     * @param boundY The height of the game screen
     */
    public MyPolygon(MyPoint[] inShape, MyPoint inPosition, double inRotation, int boundX, int boundY) {
        super(inPosition, inRotation);
        shape = inShape;

        // First, we find the shape's top-most left-most boundary, its origin.
        MyPoint origin = new MyPoint(shape[0].x, shape[0].y);//;
        for ( MyPoint p : shape) {
            if (p.x < origin.x) origin.x = p.x;
            if (p.y < origin.y) origin.y = p.y;
        }

        // Then, we orient all of its points relative to the real origin.
        for ( MyPoint p : shape) {
            p.x -= origin.x;
            p.y -= origin.y;
        }

        this.boundX = boundX;
        this.boundY = boundY;

    }

    /**
     * Returns the coordinates that define this MyPolygon. Adjusts for
     * this MyPolygon's position on the screen.
     *
     * @return MyPoint[] The set of (x,y) coordinates that define this MyPolygon
     */
    public MyPoint[] getPoints() {
        MyPoint center = findCenter();
        MyPoint[] points = new MyPoint[shape.length];
        int i =0;
        double rotation = this.getRotation();
        MyPoint position = this.getPosition();

        for (MyPoint p : shape) {
            double x = ((p.x-center.x) * Math.cos(Math.toRadians(rotation)))
                    - ((p.y-center.y) * Math.sin(Math.toRadians(rotation)))
                    + center.x/2 + position.x;
            double y = ((p.x-center.x) * Math.sin(Math.toRadians(rotation)))
                    + ((p.y-center.y) * Math.cos(Math.toRadians(rotation)))
                    + center.y/2 + position.y;
            points[i] = new MyPoint(x,y);
            i++;
        }
        return points;
    }

    /**
     * Finds the center of this MyPolygon. Uses findArea method.
     *
     * @return MyPoint The (x,y) point that is at this MyPolygon's center
     */
    private MyPoint findCenter() {
        MyPoint sum = new MyPoint(0,0);
        for (int i = 0, j = 1; i < shape.length; i++, j=(j+1)%shape.length) {
            sum.x += (shape[i].x + shape[j].x)
                    * (shape[i].x * shape[j].y - shape[j].x * shape[i].y);
            sum.y += (shape[i].y + shape[j].y)
                    * (shape[i].x * shape[j].y - shape[j].x * shape[i].y);
        }
        double area = findArea();
        return new MyPoint((int)Math.abs(sum.x/(6*area)),(int)Math.abs(sum.y/(6*area)));
    }

    /**
     * Finds the area contained inside this MyPolygon.
     *
     * @return double The area of this MyPolygon, in pixels
     */
    private double findArea() {
        double sum = 0;
        for (int i = 0, j = 1; i < shape.length; i++, j=(j+1)%shape.length) {
            sum += shape[i].x*shape[j].y-shape[j].x*shape[i].y;
        }
        return Math.abs(sum/2);
    }

    /**
     * Gets the bounds of this MyPolygon
     *
     * @return Rectangle2D The Rectangle2D
     * whose dimensions are the bounds of this MyPolygon
     */
    public Rectangle2D getBounds(){
        return getPoly().getBounds2D();
    }

    /**
     * Get the Polygon defined by this MyPolygon.<br>
     * The Polygon is simply x-points, y-points, and number of vertices.
     *
     * @return Polygon The Polygon with this MyPolygon's points, not adjusted
     * for screen orientation
     */

    protected Polygon getPoly(){
        MyPoint[] currentPoints = getPoints();
        int[] x = new int[currentPoints.length];
        int[] y = new int[currentPoints.length];

        for(int i=0;i<currentPoints.length; i++){
            x[i] = (int)currentPoints[i].x;
            y[i] = (int)currentPoints[i].y;
        }

        return new Polygon(x,y,currentPoints.length);
    }

    /**
     * Draws this MyPolygon on the screen.<br>
     * Calls getPoly() and draws the Polygon.
     *
     * @param brush For Graphics
     */
    public void draw(Graphics brush){

        brush.fillPolygon(getPoly());

    }
}