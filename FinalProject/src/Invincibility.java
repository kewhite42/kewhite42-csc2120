import java.awt.*;

/**
 * Invincibility Powerups make ship invincible for a specific duration
 *
 * @author Kaley White
 */
public class Invincibility extends Powerup {

    /**
     * Creates this in position specified by randPt
     *
     * @param randPt Position in which to draw this on the screen
     * @param myShip Ship that can collect this
     */
    public Invincibility(MyPoint randPt, Ship myShip){
        super(randPt, myShip);
        type = "invincibility";
        color = Color.cyan;
        dist = 0;
    }

    /**
     * Allows ship member to collect this
     */
    public void collect(){
        ship.setHasPowerup(true);
        ship.setHasInvincibility(true);
    }
}
