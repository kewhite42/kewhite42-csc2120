import java.awt.*;

/**
 * BigBullet Powerups increase ship's bullets' size for a specific duration
 *
 * @author Kaley White
 */
public class BigBullet extends Powerup {

    /**
     * Creates this in position specified by randPt
     *
     * @param randPt Position in which to draw this on the screen
     * @param myShip Ship that can collect this
     */
    public BigBullet(MyPoint randPt, Ship myShip){
        super(randPt, myShip);
        type = "big bullet";
        color = Color.pink;
        dist = 0;
    }

    /**
     * Allows ship member to collect this
     */
    public void collect(){
        ship.setHasPowerup(true);
        ship.setHasBigBullet(true);
    }
}
