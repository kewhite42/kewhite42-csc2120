import java.awt.*;

/**
 * InvincBullet Powerups cause ship's bullets not to disappear when they hit an asteroid
 *
 * @author Kaley White
 */
public class InvincBullet extends Powerup{

    /**
     * Creates this in position specified by randPt
     *
     * @param randPt Position in which to draw this on the screen
     * @param myShip Ship that can collect this
     */
    public InvincBullet(MyPoint randPt, Ship myShip){
        super(randPt, myShip);
        type = "big bullet";
        color = Color.magenta;
        dist = 0;
    }

    /**
     * Allows ship member to collect this
     */
    public void collect(){
        ship.setHasPowerup(true);
        ship.setHasInvincBullet(true);
    }
}
