import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 * This class creates, draws, and animates Ship objects. It also
 * manages this Ship's firing.
 *
 * @author Ania Kaczka Jennings
 * @author Kaley White
 */
public class Ship extends MyPolygon implements KeyListener{
    /**
     * True if this has any Powerup (excluding ExtraLife), false otherwise
     */
    private boolean hasPowerup = false;

    /**
     * True if this has a ScoreMult Powerup, false otherwise
     */
    private boolean hasScoreMult = false;

    /**
     * True if this has a BigBullet Powerup, false otherwise
     */
    private boolean hasBigBullet = false;

    /**
     * True if this has an InvincBullet Powerup, false otherwise
     */
    private boolean hasInvincBullet = false;

    /**
     * True if this has an Invincibility Powerup, false otherwise
     */
    private boolean hasInvincibility = false;

    /**
     * True if this Ship is rotating to the left, false otherwise
     */
    private boolean move_left = false;

    /**
     * True if this Ship is rotating to the right, false otherwise
     */
    private boolean move_right = false;

    /**
     * True if this Ship is moving forward, false otherwise
     */
    private boolean move_up = false;

    /**
     * True if this Ship has not died, false otherwise
     */
    private boolean alive = true;

    /**
     * Creates this Ship object in the center of the screen.<br>
     * Calls the MyPolygon constructor.
     *
     * @param boundX The width of the game screen
     * @param boundY The height of the game screen
     */
    public Ship(int boundX, int boundY){
        super(getShape(), new MyPoint(boundX/2,boundY/2),0, boundX,boundY);
    }

    /**
     * Gets the set of (x,y) points that defines this Ship.<br>
     * Not adjusted for position on the screen.
     *
     * @return MyPoint[] The array of points
     */
    static protected MyPoint[] getShape(){  //changed this to protected so LivesShip can access it
        MyPoint[] points = new MyPoint[4];
        points[0] = new MyPoint(0,0);
        points[1] = new MyPoint(-10, 20);
        points[3] = new MyPoint(10, 20);
        points[2] = new MyPoint(0, 15);
        return points;
    }

    /**
     * Sets alive to false.
     * If no more lives remaining, called from AsteroidsGame.
     */
    public void die(){
        alive = false;
    }

    /**
     * Gets whether this Ship is alive
     *
     * @return boolean True if alive, false otherwise
     */
    public boolean isAlive(){return alive;}

    /**
     * Getter for hasPowerup
     *
     * @return boolean Value of this.hasPowerup
     */
    public boolean getHasPowerup(){
        return hasPowerup;
    }

    /**
     * Setter for hasPowerup
     *
     * @param hasPowerup New value for this.hasPowerup
     */
    public void setHasPowerup(boolean hasPowerup){
        this.hasPowerup = hasPowerup;
    }

    /**
     * Getter for hasScoreMult
     *
     * @return boolean Value of this.hasScoreMult
     */
    public boolean getHasScoreMult(){
        return hasScoreMult;
    }

    /**
     * Setter for hasScoreMult
     *
     * @param hasScoreMult New value for this.hasScoreMult
     */
    public void setHasScoreMult(boolean hasScoreMult){
        this.hasScoreMult = hasScoreMult;
    }

    /**
     * Getter for hasBigBullet
     *
     * @return boolean Value of this.hasBigBullet
     */
    public boolean getHasBigBullet(){
        return hasPowerup;
    }

    /**
     * Setter for hasBigBullet
     *
     * @param hasBigBullet New value for this.hasBigBullet
     */
    public void setHasBigBullet(boolean hasBigBullet){
        this.hasBigBullet = hasBigBullet;
    }

    /**
     * Getter for hasInvincibility
     *
     * @return boolean Value of this.hasInvincibility
     */
    public boolean getHasInvincBullet(){
        return hasInvincBullet;
    }

    /**
     * Setter for hasInvincBullet
     *
     * @param hasInvincBullet New value for this.hasInvincBullet
     */
    public void setHasInvincBullet(boolean hasInvincBullet){
        this.hasInvincBullet = hasInvincBullet;
    }

    /**
     * Getter for hasInvincibility
     *
     * @return boolean Value of this.hasInvincibility
     */
    public boolean getHasInvincibility(){
        return hasInvincibility;
    }

    /**
     * Setter for hasInvincibility
     *
     * @param hasInvincibility New value for this.hasInvincibility
     */
    public void setHasInvincibility(boolean hasInvincibility){
        this.hasInvincibility = hasInvincibility;
    }

    /**
     * Moves the Ship on the screen.<br>
     * If moving forward, accelerates in the direction of rotation.<br>
     * Can also turn left/right.<br>
     * After moving, decelerates.<br>
     * Wraps around x- and y-axes.
     */
    @Override
    public void animate() {
        if(alive) {
            MyPoint p = this.getPosition();
            double rot = this.getRotation();
            if (move_left) {
                rot-= 2;
            }
            if (move_right) {
                rot+= 2;
            }
            if (move_up) {
                accelerate(.8);
            }

            p.x += pull.x;
            p.y += pull.y;

            pull.x *= .9;
            pull.y *= .9;

            if (p.x > this.boundX + 10) {
                p.x = 0 - 10;
            } else if (p.x < -10) {
                p.x = boundX + 10;
            }

            if (p.y > boundY + 10) {
                p.y = 0 - 10;
            } else if (p.y < -10) {
                p.y = boundY + 10;
            }

            this.setPosition(p);
            this.setRotation(rot);
        }
    }

    /**
     * Draws and fills in this Ship.<br>
     * When finished drawing, resets brush color to what it was before.
     *
     * @param brush So Graphics can be used
     */
    @Override
    public void draw(Graphics brush) {
        if(alive) {
            Color c = brush.getColor();
            brush.setColor(Color.orange);
            super.draw(brush);
            brush.setColor(c);
        }
    }

    /**
     * Draws and fills in this Ship in a different color than draw().<br>
     * When finished drawing, resets brush color to what it was before.
     *
     * @param brush So Graphics can be used
     */
    public void drawWithPowerup(Graphics brush) {
        if(alive) {
            Color c = brush.getColor();
            brush.setColor(Color.white);
            super.draw(brush);
            brush.setColor(c);
        }
    }

    /**
     * If this Ship is alive, returns a Bullet.
     * @return Bullet A new Bullet object if this Ship is alive. Null otherwise.
     */
    public Bullet shoot() {
        if(this.alive) {
            if(hasBigBullet)
                return new Bullet(this.getPosition(), this.getRotation(), 15);
            else
                return new Bullet(this.getPosition(), this.getRotation(), 5);
        }
        return null;
    }

    /**
     * No action is taken when a key is typed.
     * @param e So KeyEvent can be used
     */
    @Override
    public void keyTyped(KeyEvent e) {
    }

    /**
     * If up, left, or right arrow key is pressed,
     * sets corresponding direction boolean to true.
     *
     * @param e So KeyEvent can be used
     */
    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()){
            case KeyEvent.VK_LEFT:
                move_left =true;
                break;
            case KeyEvent.VK_RIGHT:
                move_right=true;
                break;
            case KeyEvent.VK_UP:
                move_up = true;
                break;
            default:
                break;
        }
    }

    /**
     * When up, left, or right arrow key is released,
     * sets corresponding direction boolean to false.
     *
     * @param e So KeyEvent can be used
     */
    @Override
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()){
            case KeyEvent.VK_LEFT:
                move_left =false;
                break;
            case KeyEvent.VK_RIGHT:
                move_right=false;
                break;
            case KeyEvent.VK_UP:
                move_up = false;
                break;
            default:
                break;
        }
    }

}