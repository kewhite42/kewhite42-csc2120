GAME CONTROLS:
	Start screen:       space to play game   **

	Game screen:        space to shoot **
		            up arrow to accelerate **
		            left arrow to rotate left **
		            right arrow to rotate right **

	Game over screen:   space to play again **
			    h to see high scores **

	Win screen:         space to play again **
		            h to see high scores **

	High scores screen: space to play again **


COOL FEATURES:
	
	Pity Party: 
		If you've already run into the asteroid, you can't lose a life for running into it again.
		By the Pity Party Doctrine, you win if you run into every asteroid. :(
		It's not a bug - it's a feature!
	
	Powerups:           
		ExtraLife (blue): An extra life appears on the screen.
		ScoreMult (white): Earn 20 points per asteroid shot instead of 10
		Invincibility (cyan): Be invincible to asteroids.
		BigBullet (pink): Bullets are 15 pixels in diameter instead of 5.
		InvincBullet (magenta): Bullets are not destroyed upon hitting an asteroid.
		Ship is white if it is currently using a powerup, orange if it isn't.

	
	When an asteroid is hit with a bullet, before disappearing, the asteroid turns a random color or splits into smaller asteroids.

	launch screen, game screen, win screen, high scores screen

	background image
	
	Difficulty increase:
		10 more asteroids spawn when score hits 100
		20 more asteroids spawn when score hits 200
		50 more asteroids spawn when score hits 500
		100 more asteroids spawn when score hits 1000

	background music

	bullets spell words
 
	Sounds when:
		You collect a powerup.
		You collide with an asteroid.
		You shoot.
		A bullet hits an asteroid.