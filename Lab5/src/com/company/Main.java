package com.company;

import java.io.*;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import static java.util.Collections.reverse;
import static java.util.Collections.sort;

public class Main {

    public static void main(String[] args) {
        String file = "MonteCristo.txt";
        String line;  //to hold each line read in
        Map<String, Integer> wordCts = new HashMap<String, Integer>();

        try (FileReader filereader = new FileReader(file)) {
            BufferedReader br = new BufferedReader(filereader);
            //read every line as an element of lines
            while ((line = br.readLine()) != null) {
                line = br.readLine();
                String[] splitLine = line.split("\\P{L}+");
                for(String s : splitLine) {
                    if (wordCts.containsKey(s)) {
                        int num = wordCts.get(s).intValue();
                        wordCts.put(s, num + 1);
                    } else {
                        wordCts.put(s, 1);
                    }
                }
            }


            Map<String, Integer> sortedMap = sortByValue(wordCts);

            File newFile = new File("C:\\Users\\kwhit\\Desktop\\CSC Stuff\\OO\\git\\Lab5\\WordCounts.txt");
            newFile.createNewFile();
            PrintWriter pw = new PrintWriter(new FileOutputStream("WordCounts.txt", true));
            Iterator iter2 = sortedMap.entrySet().iterator();
            for (Map.Entry<String, Integer> item : sortedMap.entrySet()) {
                //System.out.println(k);
                pw.println(item.getKey() + ": " + item.getValue());
                //pw.println("\n");
                pw.flush();
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        } catch (IOException e) {
            System.out.println("Cannot write to file.");
        }
    }


    public static <String, Integer extends Comparable<? super Integer>> Map<String, Integer> sortByValue(Map<String, Integer> map) {
        return map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (e1, e2) -> e1,
                        LinkedHashMap::new
                ));
    }
}
