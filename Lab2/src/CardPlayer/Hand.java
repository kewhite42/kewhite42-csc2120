package CardPlayer;
import CardDeck.Card;

import java.util.LinkedList;

/**
 * Created by kwhite on 7/14/2017.
 */
public class Hand {
    private LinkedList<Card> cards;

    public Hand() {
        cards = new LinkedList<Card>();
    }

    public void addCard(Card c) {
        cards.addFirst(c);
    }

    public LinkedList<Card> getCards() {
        return (LinkedList) this.cards.clone();
    }

    public void removeCard(Card c){
        this.cards.remove(c);
    }

    public int getValue() {
        int val = 0;
        for(Card card : this.cards) {
            switch(card.getFace()) {
                case A: val = val + 11;
                    break;
                case TWO: val = val + 2;
                    break;
                case THREE: val = val + 3;
                    break;
                case FOUR: val = val + 4;
                    break;
                case FIVE: val = val + 5;
                    break;
                case SIX: val = val + 6;
                    break;
                case SEVEN: val = val + 7;
                    break;
                case EIGHT: val = val + 8;
                    break;
                case NINE: val = val + 9;
                    break;
                case TEN:
                case J:
                case Q:
                case K: val = val + 10;
            };
        }
        return val;
    }

    public LinkedList<Card> returnHand() {
        LinkedList<Card> cards2 = this.cards;
        this.cards.clear();
        return cards2;
    }

    public String toString() {
        String str = "";
        for(Card card : this.cards) {
            str = str + card.toString() + " ";
        }
        return str;
    }
}