package CardPlayer;

import CardDeck.Card;

import java.util.LinkedList;

/**
 * Created by kwhite on 7/14/2017.
 */
public class Player {
    protected Hand hand = new Hand();
    protected String name;

    public Player() {
        this.hand = new Hand();
    }

    public void addToHand(Card c) {
        this.hand.addCard(c);
    }

    public void removeFromHand(Card c) {this.hand.removeCard(c);}

    public int getHandValue() {
        return hand.getValue();
    }

    public String getHand() {
        return hand.toString();
    }

    public int handSize() {
        return hand.getCards().size();
    }
}