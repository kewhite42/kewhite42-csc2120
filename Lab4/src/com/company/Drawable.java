package com.company;

import java.awt.*;

/**
 * Created by kwhite on 7/19/2017.
 */
public interface Drawable {
    void animate(Graphics brush);
    void draw(Graphics brush);
}
