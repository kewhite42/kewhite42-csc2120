package com.company; /**
 * Created by kwhite on 7/17/2017.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public abstract class Game extends JPanel {
    protected int width;
    protected int height;
    protected JFrame frame;

    public Game(String name, int w, int h){
        this.width = w;
        this.height = h;
        Frame frame = new JFrame(name);
        frame.add(this);
        frame.setSize(w, h);
        frame.setVisible(true);
        frame.setResizable(false);
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e){System.exit(0);}
        });
    }

    public abstract void paint(Graphics brush);
}
