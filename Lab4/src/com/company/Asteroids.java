package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

public class Asteroids extends Game implements ActionListener {
    protected Timer timer = new Timer(10, this);
    protected ArrayList<Drawable> drawables = new ArrayList<Drawable>();
    protected Random r = new Random(System.currentTimeMillis());

    public Asteroids(String name, int w, int h){

        super(name, w, h);
        timer.start();
        MyPolygon tri = new MyPolygon(new MyPoint[] {new MyPoint(600, 150), new MyPoint(600, 200), new MyPoint(450, 175)},
                new MyPoint(200, 200), 0, 800, 600);
        drawables.add(tri);

        MyPolygon quad = new MyPolygon(new MyPoint[] {new MyPoint(60, 100), new MyPoint(200, 210), new MyPoint(300, 115), new MyPoint(95, 50)},
                new MyPoint(550,500), 0, 800, 600);
        drawables.add(quad);

        MyPolygon pent = new MyPolygon(new MyPoint[] {new MyPoint(100, 50), new MyPoint(130, 50), new MyPoint(125, 30), new MyPoint(100,25), new MyPoint(85,40)},
                new MyPoint(100, 450), 0, 800, 600);
        drawables.add(pent);

        Ship s = new Ship(800, 600);
        drawables.add(s);
        System.out.println("drew all polygons");

        //I could not get the Asteroids to be drawn

        /*MyPoint[] smallShape1 = {new MyPoint(600, 150), new MyPoint(600, 200), new MyPoint(450, 175)};
        Asteroid small1 = new Asteroid(smallShape1, new MyPoint((double)r.nextInt(w), (double)r.nextInt(h)), (double)r.nextInt(360), w, h);
        drawables.add(small1);*/

        /*Asteroid small2 = new Asteroid(smallShape2, new MyPoint((double)r.nextInt(w), (double)r.nextInt(h)), (double)r.nextInt(360), w, h);
        drawables.add(small2);

        Asteroid small3 = new Asteroid(smallShape3, new MyPoint((double)r.nextInt(w), (double)r.nextInt(h)), (double)r.nextInt(360), w, h);
        drawables.add(small3);*/

       /* MyPoint[] medShape1 = {new MyPoint(500, 50), new MyPoint(550, 150), new MyPoint(200, -25)};
        Asteroid med1 = new Asteroid(medShape1, new MyPoint((double)r.nextInt(w), (double)r.nextInt(h)), (double)r.nextInt(360), w, h);
        drawables.add(med1);*/

       /* Asteroid med2 = new Asteroid(medShape2, new MyPoint((double)r.nextInt(w), (double)r.nextInt(h)), (double)r.nextInt(360), w, h);
        drawables.add(med2);

        Asteroid med3 = new Asteroid(medShape3, new MyPoint((double)r.nextInt(w), (double)r.nextInt(h)), (double)r.nextInt(360), w, h);
        drawables.add(med3);*/

        /*MyPoint[] largeShape1 = {new MyPoint(700, 250), new MyPoint(700, 300), new MyPoint(550, 275)};
        Asteroid large1 = new Asteroid(largeShape1, new MyPoint((double)r.nextInt(w), (double)r.nextInt(h)), (double)r.nextInt(360), w, h);
        drawables.add(large1);/*

        /*Asteroid large2 = new Asteroid(largeShape2, new MyPoint((double)r.nextInt(w), (double)r.nextInt(h)), (double)r.nextInt(360), w, h);
        drawables.add(large2);

        Asteroid large3 = new Asteroid(largeShape3, new MyPoint((double)r.nextInt(w), (double)r.nextInt(h)), (double)r.nextInt(360), w, h);
        drawables.add(large3);*/
    }

    public void paint(Graphics brush){
        int w = this.width;
        brush.setColor(Color.black);
        brush.fillRect(0,0, this.width, this.height);

        for(Drawable d : drawables){
            d.animate(brush);
            System.out.println("animated");
            d.draw(brush);
        }
    }

    public void actionPerformed(ActionEvent ae){

        try {
            ae.equals(null);
        }catch(NullPointerException e) {
            System.out.println(e);
        }

        frame.repaint();
    }

    public static void main(String[] args) {
	    String n = "Asteroids";
	    int w = 800;
	    int h = 600;
        Asteroids asteroids = new Asteroids(n, w, h);
        System.out.println("entering Asteroids constructor");
    }
}
