package com.company;

import java.awt.*;
import java.lang.reflect.Array;

/**
 * Created by kwhite on 7/19/2017.
 */
public class Ship extends MyPolygon {
    Ship(int inBoundX, int inBoundY) {
        super(new MyPoint[]{new MyPoint(inBoundX/2, inBoundY/2), new MyPoint(inBoundX/2 - 30, inBoundY/2 - 18), new MyPoint(inBoundX/2 + 45, inBoundY/2), new MyPoint(inBoundX/2 - 30, inBoundY/2 + 18)},
                new MyPoint(400, 300), 0, inBoundX, inBoundY);
    }

    @Override
    public void draw(Graphics brush) {
        Color c = brush.getColor();
        brush.setColor(Color.pink);

        MyPoint[] pts = getPoints();
        int sz = Array.getLength(pts);
        int[] x_pts = new int[sz];
        int[] y_pts = new int[sz];

        //get x-coordinates & y-coordinates from shape
        for (int i = 0; i < Array.getLength(pts); i++) {
            x_pts[i] = (int) pts[i].x;
            y_pts[i] = (int) pts[i].y;
        }

        //draw the polygon defined by shape
        brush.fillPolygon(x_pts, y_pts, Array.getLength(pts));

        brush.setColor(c);
    }

    @Override
    public void animate(Graphics brush) {
        //go out left (keep decrementing values in x_pts)
        MyPoint[] ship = getPoints();
        int sz = Array.getLength(ship);
        double[] x_pts = new double[sz];
        double[] y_pts = new double[sz];

        for (int i = 0; i < 4; i++) {
            x_pts[i] = ship[i].x;
            y_pts[i] = ship[i].y;
        }

        while (x_pts[2] > 0) {
            for (int i = 0; i < 4; i++)
                x_pts[i]--;
        }


            //come back in right
            rotation = 180;
            //clear array for new points
            for (int i = 0; i < 4; i++)
                x_pts[i] = 0;
            //position ship ready to come in from right
            x_pts[0] = boundX;
            x_pts[1] = boundX + 25;
            x_pts[2] = boundX + 15;
            x_pts[3] = boundX + 25;
    }
    }