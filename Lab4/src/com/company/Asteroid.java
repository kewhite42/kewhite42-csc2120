package com.company;

import java.awt.*;
import java.lang.reflect.Array;

/**
 * Created by kwhite on 7/19/2017.
 */
public class Asteroid extends MyPolygon {

    //how to specify size in MyPolygon constructor?
    Asteroid(MyPoint[] inShape, MyPoint inPosition, double inRotation, int inBoundX, int inBoundY) {
        super(inShape, inPosition, inRotation, inBoundX, inBoundY);
    }

    @Override
    public void draw(Graphics brush) {
        Color c = brush.getColor();
        brush.setColor(Color.gray);

        MyPoint[] shape = this.getPoints();
        int sz = Array.getLength(shape);
        int[] x_pts = new int[sz];
        int[] y_pts = new int[sz];

        //get x-coordinates & y-coordinates from shape
        for (int i = 0; i < Array.getLength(shape); i++) {
            x_pts[i] = (int) shape[i].x;
            y_pts[i] = (int) shape[i].y;
        }

        //draw the polygon defined by shape
        brush.drawPolygon(x_pts, y_pts, Array.getLength(shape));

        brush.setColor(c);
    }

    public void animate(Graphics brush) {
        //go out left (keep decrementing values in x_pts)
        MyPoint[] asteroid = getPoints();
        int sz = Array.getLength(asteroid);
        double[] x_pts = new double[sz];
        double[] y_pts = new double[sz];
        //need to keep track of original position for when asteroid comes back in from right
        double[] x_ptsOrig = new double[sz];
        double[]y_ptsOrig = new double[sz];

        for (int i = 0; i < sz; i++) {
            x_pts[i] = asteroid[i].x;
            y_pts[i] = asteroid[i].y;
            x_ptsOrig[i] = asteroid[i].x;
            y_ptsOrig[i] = asteroid[i].y;
        }

        //go out left
        //If rightmost x-coordinate is offscreen, they all are.
        int rightmostIndex = 0;
        for(int i = 1; i < sz; i++) {
            if (x_pts[i] < x_pts[rightmostIndex])
                rightmostIndex = i;
        }
        boolean allOffscreenX = false;
        while (!allOffscreenX) {
            draw(brush);
            //decrement x-coordinates
            for (int i = 0; i < sz; i++) {
                x_pts[i]--;
            }
            //are all x-points offscreen?
            if(x_pts[rightmostIndex] < 0)
                allOffscreenX = true;
        }


        //come back in right
        //leftmost point should come in first
        int leftmostIndex = 0;
        for(int i = 1; i < sz; i++){
            if(x_pts[i] < x_pts[leftmostIndex])
                leftmostIndex = i;
        }
        double refX = x_pts[leftmostIndex];
        double[] x_ptsRef = new double[sz];
        for(int i = 0; i < sz; i++) {
            x_ptsRef[i] = x_pts[i] - refX;
        }
        //position asteroid ready to come in from right
        x_pts[leftmostIndex] = boundX;
        for(int i = 0; i < sz; i++){
            x_pts[i] = boundX + x_ptsRef[i];
        }

        boolean inOrigPosX = false;
        while (!inOrigPosX) {
            draw(brush);
            //decrement x-coordinates
            for (int i = 0; i < sz; i++) {
                x_pts[i]--;
            }
            //Is reference coordinate back where it started?
            //If so, the rest are, too (ASSUMING NO ROTATION).
            if(x_pts[0] == x_ptsOrig[0])
                inOrigPosX = true;
        }


        //go out bottom
        //If topmost point is offscreen, they all are.
        int topmostIndex = 0;
        for(int i = 1; i < sz; i++){
            if(x_pts[i] < x_pts[topmostIndex])
                topmostIndex = i;
        }
        boolean allOffscreenY = false;
        while (!allOffscreenY) {
            draw(brush);
            //decrement y-coordinates
            for (int i = 0; i < sz; i++) {
                y_pts[i]--;
            }
            //are all y-points offscreen?
            if(x_pts[topmostIndex] < 0)
                allOffscreenY = true;
        }


        //come back in at top
        double refY = y_pts[leftmostIndex];
        double[]y_ptsRef = new double[sz];
        for(int i = 0; i < sz; i++) {
            y_ptsRef[i] = y_pts[i] - refY;
        }
        //clear array for new points
        /*for (int i = 0; i < sz; i++)
            y_pts[i] = 0;*/
        //position ship ready to come in from right
        y_pts[leftmostIndex] = boundY;
        for(int i = 1; i < sz; i++){
            y_pts[i] = boundY + y_ptsRef[i];
        }

        boolean inOrigPos = false;
        while (!inOrigPos) {
            draw(brush);
            //decrement y-coordinates
            for (int i = 0; i < sz; i++) {
                y_pts[i]--;
            }
            //Is reference coordinate back where it started?
            //If so, the rest are, too (ASSUMING NO ROTATION).
            if(y_pts[0] == y_ptsOrig[0])
                inOrigPos = true;
        }
    }
}
