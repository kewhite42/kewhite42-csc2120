/**
 * Created by Ania on 7/15/2017.
 */
import java.awt.*;

public class MyPolygon {

    private MyPoint[] shape;   // An array of points.
    public MyPoint position;   // The offset mentioned above.
    public double rotation; // Zero degrees is due east.

    public MyPolygon(MyPoint[] inShape, MyPoint inPosition, double inRotation) {
        shape = inShape;
        position = inPosition;
        rotation = inRotation;

        // First, we find the shape's top-most left-most boundary, its origin.
        MyPoint origin = new MyPoint(shape[0].x, shape[0].y);//;
        for ( MyPoint p : shape) {
            if (p.x < origin.x) origin.x = p.x;
            if (p.y < origin.y) origin.y = p.y;
        }

        // Then, we orient all of its points relative to the real origin.
        for ( MyPoint p : shape) {
            p.x -= origin.x;
            p.y -= origin.y;
        }
    }

    public boolean contains( MyPoint point) {
        MyPoint[] points = getPoints();
        double crossingNumber = 0;
        for (int i = 0, j = 1; i < shape.length; i++, j=(j+1)%shape.length) {
            if ((((points[i].x < point.x) && (point.x <= points[j].x)) ||
                    ((points[j].x < point.x) && (point.x <= points[i].x))) &&
                    (point.y > points[i].y + (points[j].y-points[i].y)/
                            (points[j].x - points[i].x) * (point.x - points[i].x))) {
                crossingNumber++;
            }
        }
        return crossingNumber%2 == 1;
    }
    public MyPoint[] getPoints() {
        MyPoint center = findCenter();
        MyPoint[] points = new MyPoint[shape.length];
        int i =0;
        for (MyPoint p : shape) {
            double x = ((p.x-center.x) * Math.cos(Math.toRadians(rotation)))
                    - ((p.y-center.y) * Math.sin(Math.toRadians(rotation)))
                    + center.x/2 + position.x;
            double y = ((p.x-center.x) * Math.sin(Math.toRadians(rotation)))
                    + ((p.y-center.y) * Math.cos(Math.toRadians(rotation)))
                    + center.y/2 + position.y;
            points[i] = new MyPoint(x,y);
            i++;
        }
        return points;
    }

    private MyPoint findCenter() {
        MyPoint sum = new MyPoint(0,0);
        for (int i = 0, j = 1; i < shape.length; i++, j=(j+1)%shape.length) {
            sum.x += (shape[i].x + shape[j].x)
                    * (shape[i].x * shape[j].y - shape[j].x * shape[i].y);
            sum.y += (shape[i].y + shape[j].y)
                    * (shape[i].x * shape[j].y - shape[j].x * shape[i].y);
        }
        double area = findArea();
        return new MyPoint((int)Math.abs(sum.x/(6*area)),(int)Math.abs(sum.y/(6*area)));
    }

    private double findArea() {
        double sum = 0;
        for (int i = 0, j = 1; i < shape.length; i++, j=(j+1)%shape.length) {
            sum += shape[i].x*shape[j].y-shape[j].x*shape[i].y;
        }
        return Math.abs(sum/2);
    }

    public void paint(Graphics brush){
        //TODO
    }

}
