import javafx.scene.shape.Circle;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

/**
 * This class creates and allows manipulation of MyCircle objects
 *
 * @author Ania Kaczka Jennings
 */
public abstract class MyCircle extends MyShape {

    /**
     * The double that represents the radius of this MyCircle
     */
    private double radius;

    /**
     * Creates this MyCircle of specified position, rotation, and size.<br>
     * Calls the MyShape constructor.
     *
     * @param position The location of this circle on the screen
     * @param rotation The orientation of this circle on the screen
     * @param radius The radius of this circle
     */
    protected MyCircle(MyPoint position, double rotation, double radius) {
        super(position, rotation);
        this.radius = radius;
    }

    /**
     * Draws this MyCircle on the screen.
     *
     * @param brush So Graphics can be used
     */
    public void draw(Graphics brush) {
        brush.drawOval((int) this.getPosition().x, (int) this.getPosition().y, (int) radius, (int) radius);
    }

    /**
     * Gets the bounds of this MyCircle
     *
     * @return Rectangle2D The width and height of the returned value
     * are the bounds of this MyCircle.
     */
    @Override
    public Rectangle2D getBounds() {
        Shape circle = new Ellipse2D.Double(this.getPosition().x, this.getPosition().y, this.radius,this.radius);
        return circle.getBounds2D();
    }
}