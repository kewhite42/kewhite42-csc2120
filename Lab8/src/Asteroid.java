import java.awt.*;
import java.util.Random;

/**
 * This class creates, draws, and animates an Asteroid object.
 *
 * @author Ania Kaczka Jennings
 */
public class Asteroid extends MyPolygon{
    /**
     * Possible types of Asteroids
     */
    public enum ROCK {
        /**
         * large, shape 1
         */
        L1,

        /**
         * large, shape 2
         */
        L2,

        /**
         * medium, shape 1
         */
        M1,

        /**
         * medium, shape 2
         */
        M2,

        /**
         * small, shape 1
         */
        S1,

        /**
         * small, shape 2
         */
        S2};

    /**
     * Direction of rotation<br>
     * -1 for right, 1 for left
     */
    private double rotDelat = -1;

    /**
     * Creates this Asteroid object of specified type, position, and rotation.<br>
     * Calls the MyPolygon constructor.<br>
     * Initializes rotDelat to 1 or -1 (random).<br>
     *
     * @param type The type of Asteroid to be created
     * @param inPosition Location on the screen in which to draw this Asteroid
     * @param inRotation Orientation of this Asteroid
     * @param X Width of the game screen
     * @param Y Height of the game screen
     */
    Asteroid(ROCK type, MyPoint inPosition, double inRotation, int X, int Y) {
        super(getShape(type),inPosition, inRotation, X, Y);
        this.accelerate(.8);

        Random rand = new Random();
        rand.setSeed(System.currentTimeMillis());

        this.rotDelat = rand.nextDouble()%2.0;
        if(rand.nextInt(1) == 0){
            this.rotDelat *= -1;
        }

    }

    /**
     * Draws (but does not fill in) this Asteroid in white.<br>
     * When finished drawing, changes the color of the brush to what it was before.
     *
     * @param brush For Graphics
     */
    @Override
    public void draw(Graphics brush) {
        Color c = brush.getColor();
        brush.setColor(Color.WHITE);
        brush.drawPolygon(this.getPoly());
        brush.setColor(c);
    }

    /**
     * Moves this Asteroid on the screen.<br>
     * Moves one pixel up or one pixel down at a time.<br>
     * Rotates in direction specified by rotDelat.<br>
     * This Asteroid accelerates as it moves.<br>
     * Wraps around x- and y-axes.
     */
    @Override
    public void animate() {

        MyPoint p = this.getPosition();
        p.x += pull.x;
        p.y += pull.y;

        double rot = this.getRotation();
        rot += rotDelat;
        this.setRotation(rot);

        if (p.x > this.boundX + 10){
            p.x = 0 - 10;
        } else if (p.x < -10){
            p.x = boundX+10;
        }

        if(p.y > boundY +10){
            p.y = 0- 10;
        } else if (p.y < -10){
            p.y = boundY+10;
        }

        this.setPosition(p);
    }


    /**
     * Gets the (x,y) coordinates that define the shape to be drawn.
     *
     * @param type The type of this Asteroid
     * @return MyPoint[] The array of (x,y) points
     */
    static MyPoint[] getShape(ROCK type){

        MyPoint[] pointArray;
        switch (type) {
            case M1:
                pointArray = new MyPoint[5];
                pointArray[0] = new MyPoint(0,0);
                pointArray[1] = new MyPoint(20,-10);
                pointArray[2] = new MyPoint(25,-15);
                pointArray[3] = new MyPoint(10,-20);
                pointArray[4] = new MyPoint(-10,-10);
                break;
            case M2:
                pointArray = new MyPoint[5];
                pointArray[0] = new MyPoint(0,0);
                pointArray[1] = new MyPoint(10,-5);
                pointArray[2] = new MyPoint(25,-15);
                pointArray[3] = new MyPoint(5,-20);
                pointArray[4] = new MyPoint(-5,-10);
                break;
            case S1:
                pointArray = new MyPoint[5];
                pointArray[0] = new MyPoint(0,0);
                pointArray[1] = new MyPoint(30,-10);
                pointArray[2] = new MyPoint(35,-20);
                pointArray[3] = new MyPoint(20,-30);
                pointArray[4] = new MyPoint(-10,-10);
                break;
            case S2:
                pointArray = new MyPoint[5];
                pointArray[0] = new MyPoint(0,0);
                pointArray[1] = new MyPoint(30,-5);
                pointArray[2] = new MyPoint(35,-10);
                pointArray[3] = new MyPoint(10,-20);
                pointArray[4] = new MyPoint(-10,-10);
                break;
            case L1:
                pointArray = new MyPoint[5];
                pointArray[0] = new MyPoint(0,0);
                pointArray[1] = new MyPoint(30,-10);
                pointArray[2] = new MyPoint(35,-30);
                pointArray[3] = new MyPoint(20,-40);
                pointArray[4] = new MyPoint(-20,-10);
                break;
            case L2:
                pointArray = new MyPoint[6];
                pointArray[0] = new MyPoint(0,0);
                pointArray[1] = new MyPoint(30,-10);
                pointArray[2] = new MyPoint(35,-40);
                pointArray[3] = new MyPoint(20,-40);
                pointArray[4] = new MyPoint(-15,-20);
                pointArray[5] = new MyPoint(-5,-2);
                break;
            default:
                pointArray = new MyPoint[5];
                pointArray[0] = new MyPoint(0,0);
                pointArray[1] = new MyPoint(20,-10);
                pointArray[2] = new MyPoint(25,-15);
                pointArray[3] = new MyPoint(10,-20);
                pointArray[4] = new MyPoint(-10,-10);

                break;
        }
        return pointArray;

    }
}