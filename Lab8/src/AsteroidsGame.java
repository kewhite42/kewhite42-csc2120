import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

/**
 * This class creates a new AsteroidsGame that responds to keyboard input
 * and redraws the screen at intervals.
 *
 * @author Ania Kaczka Jennings
 */
class AsteroidsGame extends Game implements ActionListener, KeyListener {
    /**
     * Timer that allows this to be redrawn every 10 ms
     */
    protected Timer timer = new Timer(10, this);

    /**
     * True if the player can shoot, false otherwise
     */
    private boolean can_fire_shot = true;

    /**
     * List of Asteroids on this screen
     */
    private ArrayList<Asteroid> asteroids = new ArrayList<>();

    /**
     * List of Bullets on this screen
     */
    private ArrayList<Bullet> bullets = new ArrayList<>();

    /**
     * The Ship that the player controls to play this game
     */
    private Ship playerShip;

    /**
     * Allows randomness when Asteroids are spawned
     */
    private Random rand;

    /**
     * Creates this AsteroidsGame.<br>
     * Calls the Game constructor.<br>
     * Creates playerShip, spawns Asteroids, and starts the timer.
     */
    public AsteroidsGame() {
        super("Asteroids!", 800, 600);
        rand = new Random();
        rand.setSeed(System.currentTimeMillis());

        playerShip = new Ship(this.width, this.height);
        this.frame.addKeyListener(playerShip);
        this.frame.addKeyListener(this);

        spawnAsteroids(10);
        timer.start();
    }

    /**
     * Populates this screen with the given number of Asteroids.<br>
     * Asteroids are of random type and random position on this screen.
     *
     * @param count The number of Asteroids to spawn
     */
    public void spawnAsteroids(int count) {
        for (int i = 0; i < count; i++) {
            int val = rand.nextInt(Asteroid.ROCK.values().length);
            Asteroid.ROCK type = Asteroid.ROCK.values()[val];
            int x = rand.nextInt(width);
            int y = rand.nextInt(height);

            if (x < width / 2 + 100 && x > width / 2 - 100) {
                x += 200;
            }
            if (y < height / 2 + 100 && y > height / 2 - 100) {
                y += 200;
            }


            asteroids.add(new Asteroid(type, new MyPoint(x, y), rand.nextInt() % 360, width, height));
        }
    }

    /**
     * Draws playerShip, Asteroids, and Bullets on this screen.<br>
     * Before drawing, removes objects that collide.
     *
     * @param brush So Graphics can be used
     */
    public void paint(Graphics brush) {
        brush.setColor(Color.black);
        brush.fillRect(0, 0, this.width, this.height);
        brush.setColor(Color.BLUE);

        for (Asteroid obj : asteroids) {
            obj.animate();
            obj.draw(brush);
            if (playerShip.collision(obj)) {
                playerShip.die();
            }
        }

        ArrayList<Bullet> bullets_to_remove = new ArrayList<>();
        ArrayList<Asteroid> ast_to_remove = new ArrayList<>();
        for (Bullet b : bullets
                ) {
            b.animate();
            b.draw(brush);
            if (b.getPosition().x > width || b.getPosition().y > height || b.getPosition().x < 0 || b.getPosition().y < 0) {
                bullets_to_remove.add(b);
            }

            for (Asteroid obj : asteroids
                    ) {
                if (obj.collision(b)) {
                    bullets_to_remove.add(b);
                    ast_to_remove.add(obj);
                }
            }
        }

        bullets.removeAll(bullets_to_remove);
        asteroids.removeAll(ast_to_remove);

        playerShip.animate();
        playerShip.draw(brush);
    }

    /**
     * Calls this AsteroidsGame constructor.
     *
     * @param args Unused
     */
    public static void main(String[] args) {
        new AsteroidsGame();
    }

    /**
     * Updates the game screen.
     *
     * @param e So ActionEvent can be used
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        frame.repaint();
    }

    /**
     * No action is taken when a key is typed.
     *
     * @param e So KeyEvent can be used
     */
    @Override
    public void keyTyped(KeyEvent e) {

    }

    /**
     * If player can shoot, fires ONE bullet per space key pressed.
     *
     * @param e So KeyEvent can be used
     */
    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_SPACE:
                if (can_fire_shot) {
                    Bullet b = playerShip.shoot();
                    if (b != null) {
                        bullets.add(b);
                    }
                }
                can_fire_shot = false;
                break;
        }
    }

    /**
     * Updates canShoot to true when space bar is released.
     *
     * @param e So KeyEvent can be used
     */
    @Override
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_SPACE:
                //playerShip.shoot();
                can_fire_shot = true;
                break;
        }
    }
}