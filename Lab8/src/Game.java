import javax.swing.*;
import java.awt.*;

/**
 * This class creates a Game object defined by a JFrame.
 *
 * @author Ania Kaczka Jennings
 */
abstract class Game extends JPanel {

    /**
     * The int width and height, respectively, of this screen
     */
    protected int width, height;

    /**
     * Allows this game screen to be drawn and manipulated
     */
    protected JFrame frame;

    /**
     * Creates this Game with specified name and size.
     *
     * @param name Displayed at the top of the JFrame
     * @param inWidth The width of the JFrame that defines this Game
     * @param inHeight The height of the JFrame that defines this Game
     */
    public Game(String name, int inWidth, int inHeight) {
        width = inWidth;
        height = inHeight;

        frame = new JFrame(name);
        frame.add(this);
        frame.setSize(width,height);
        frame.setVisible(true);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

    /**
     * Draws this game screen.<br>
     * Classes that extend Game will have Timer object, and paint
     * will be called every x milliseconds, where x is the delay of the timer.
     *
     * @param brush So Graphics can be used
     */
    abstract public void paint(Graphics brush);

}