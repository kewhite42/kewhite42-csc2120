package com.company; /**
 * Created by kwhite on 7/17/2017.
 */
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public abstract class Game extends Canvas {
    protected boolean on;
    protected int width;
    protected int height;
    protected Image buffer;

    public Game(String name, int w, int h){
        this.on = true;
        this.width = w;
        this.height = h;
        Frame frame = new Frame(name);
        frame.add(this);
        frame.setSize(w, h);
        frame.setVisible(true);
        frame.setResizable(false);
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e){System.exit(0);}
        });
        this.buffer = createImage(w, h);
    }

    public abstract void paint(Graphics brush);

    private void sleep(int time) {
        try{Thread.sleep(time);} catch(Exception exc){};
    }

    public void update(Graphics brush){
        paint(buffer.getGraphics());
        brush.drawImage(this.buffer,0,0,this);
        if(on){
            sleep(10);
            repaint();
        }
    }
}
