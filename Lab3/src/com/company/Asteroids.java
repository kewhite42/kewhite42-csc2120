package com.company;

import java.awt.*;
import static java.awt.Color.black;

public class Asteroids extends Game {
    public Asteroids(String name, int w, int h){
        super(name, w, h);
    }

    public void paint(Graphics brush){
        int w = this.width;
        brush.setColor(Color.black);
        brush.fillRect(0,0, this.width, this.height);

        brush.setColor(Color.yellow);
        brush.fill3DRect(this.width/4,this.height/4,this.width/2,this.height/2,true);

        brush.setColor(Color.orange);
        brush.fill3DRect(this.width/4+25,this.height/4+25,this.width/2-50,this.height/2-50,true);

        brush.setColor(Color.yellow);
        brush.fill3DRect(this.width/4+50,this.height/4+50,this.width/2-100,this.height/2-100,true);

        brush.setColor(Color.orange);
        brush.fill3DRect(this.width/4+75,this.height/4+75,this.width/2-150,this.height/2-150,true);

        brush.setColor(Color.red);
        brush.fillOval(this.width/2-50, this.height/2-50, 100, 100);

        brush.setColor(Color.yellow);
        brush.fillOval(this.width/2-25, this.height/2-25, 50, 50);

        brush.setColor(Color.red);
        brush.fillOval(this.width/2-12, this.height/2-12, 25, 25);

        brush.setColor(Color.green);
        int[] x_ptsLeftTop = {0, this.width/10, 0};
        int[] y_ptsLeftTop = {0, 0, this.height/10};
        Polygon triLeftTop = new Polygon(x_ptsLeftTop, y_ptsLeftTop, 3);
        brush.fillPolygon(triLeftTop);

        int[] x_ptsRightTop = {this.width-this.width/10, this.width, this.width};
        int[] y_ptsRightTop = {0, 0, this.height/10};
        Polygon triRightTop = new Polygon(x_ptsRightTop, y_ptsRightTop, 3);
        brush.fillPolygon(triRightTop);

        int[] x_ptsLeftBottom = {0, 0, this.width/10};
        int[] y_ptsLeftBottom = {this.height-this.height/10,this.height,this.height};
        Polygon triLeftBottom = new Polygon(x_ptsLeftBottom, y_ptsLeftBottom, 3);
        brush.fillPolygon(triLeftBottom);

        int[] y_ptsRightBottom = {this.height, this.height-this.height/10, this.height};
        int[] x_ptsRightBottom = {this.width-this.width/10,this.width,this.width};
        Polygon triRightBottom = new Polygon(x_ptsRightBottom, y_ptsRightBottom, 3);
        brush.fillPolygon(triRightBottom);
    }

    public static void main(String[] args) {
	    String n = "Asteroids";
	    int w = 800;
	    int h = 600;
        Asteroids asteroids = new Asteroids(n, w, h);
    }
}
